#include "render_program.h"
#include "NRD_HAL.h"
#include <stdio.h>
#include <stdlib.h>

#define BUFFER_OFFSET(i) ((char *)NULL + (i))
#define STRINGIFY(x) #x

static const char *basicVertexShader =
#include "shaders/basic.vert"

static const char *basicImagefragmentShader =
#include "shaders/basic.frag"

static const char *basicTextfragmentShader =
#include "shaders/text.frag"

static const char *basicVinfragmentShader =
#include "shaders/vin.frag"

static const char *zoltarVertexShader =
#include "shaders/zoltar.vert"

static const char *zoltarFragmentShader =
#include "shaders/zoltar.frag"

static const char *videoFragmentShader =
#include "shaders/basic_video.frag"

static const char *highlightFragmentShader =
#include "shaders/highlight.frag"

static const char *tileVertShader =
#include "shaders/tile.vert"

static const char *tileFragmentShader =
#include "shaders/tile.frag"

static const char *playbackFragmentShader =
#include "shaders/playback.frag"

typedef struct {
float x, y, u, v;
} VERTEX;

static VERTEX *Vertices;
static int NumVertices;

void GenerateS() {
    
    int cols = 6;
    int i =0;
    int idx = 0;

    NumVertices = (cols+1)*2;
    Vertices = mem.calloc (NumVertices, sizeof(VERTEX));

    for (i=0;i<=cols;i++) {
        float p = (float)i/cols;
        Vertices[idx].x = p;
        Vertices[idx].y = 0;
        Vertices[idx].u = p;
        Vertices[idx].v = 0;

        idx++;
        Vertices[idx].x = p;
        Vertices[idx].y = -1;
        Vertices[idx].u = p;
        Vertices[idx].v = 1;
        idx++;
    } 
}


void printShaderInfoLog(GLuint shader) {

    GLint compiled = GL_FALSE;
    gl.glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

    printf("compiled? %i\n", compiled);

    int infologLen = 0;
    int charsWritten  = 0;
    char *infoLog;

    gl.glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infologLen);
    //gl.printOpenGLError();  // Check for OpenGL errors
  
    if (infologLen > 0)
    { 
        infoLog = (GLchar*) malloc(infologLen);
        if (infoLog == NULL)
        {
            printf("ERROR: Could not allocate InfoLog buffer\n");
            exit(1);
        }
        gl.glGetShaderInfoLog(shader, infologLen, &charsWritten, infoLog);
        printf("InfoLog:\n%s\n\n", infoLog);
        free(infoLog);
    }
}


GLuint compileProgram( const char *vs_src,  const char *fs_src) {

    GLuint vs = gl.glCreateShader(GL_VERTEX_SHADER);
    gl.glShaderSource(vs, 1, &vs_src, NULL);
    gl.glCompileShader(vs);
    printShaderInfoLog(vs);    
    
    #ifdef _NEXUS
        const char* fsrc[2] = {"precision mediump float;", fs_src};
    #else 
        const char* fsrc[2] = {"", fs_src};
    #endif

    GLuint fs = gl.glCreateShader(GL_FRAGMENT_SHADER);
    gl.glShaderSource(fs, 2, &fsrc[0], NULL);
    gl.glCompileShader(fs);
    
    printShaderInfoLog(fs);
    GLuint program = gl.glCreateProgram();
    gl.glUseProgram(program);

    gl.glAttachShader(program, vs);
    gl.glAttachShader(program, fs);
    gl.glLinkProgram(program);

    GLint linked = GL_FALSE;

    GLuint texAttr = gl.glGetAttribLocation(program, "texCoord0");
    GLuint posAttr = gl.glGetAttribLocation(program, "position");

    gl.glEnableVertexAttribArray(texAttr);
    gl.glEnableVertexAttribArray(posAttr);
    gl.glVertexAttribPointer(posAttr, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), BUFFER_OFFSET(0));
    gl.glVertexAttribPointer(texAttr, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), BUFFER_OFFSET(2 * sizeof(GLfloat)));

    printf("programe %u\n", program);
    return program;
}

#define get_uniform(shader, prop) renderData->program_##shader##_##prop = gl.glGetUniformLocation( renderData->program_##shader, #prop ); printf("%s %s %i\n", #shader, #prop, gl.glGetUniformLocation( renderData->program_##shader, #prop ) );

void makeProgram(struct RenderData *renderData) {

    GenerateS();
    GLuint vbo;

    renderData->num_verts = NumVertices;

    GLuint attr_array;
    gl.glGenBuffers(1, &attr_array);
    gl.glBindBuffer(GL_ARRAY_BUFFER, attr_array);
    gl.glBufferData(GL_ARRAY_BUFFER, sizeof(VERTEX) * NumVertices, Vertices, GL_STATIC_DRAW);

    renderData->program_highlight  = compileProgram(basicVertexShader,  highlightFragmentShader);
    renderData->program_img        = compileProgram(basicVertexShader,  basicImagefragmentShader);
    renderData->program_playback   = compileProgram(basicVertexShader,  playbackFragmentShader);
    renderData->program_text       = compileProgram(basicVertexShader,  basicTextfragmentShader);
    renderData->program_vin        = compileProgram(basicVertexShader,  basicVinfragmentShader);
    renderData->program_zoltar     = compileProgram(zoltarVertexShader, zoltarFragmentShader);
    renderData->program_video      = compileProgram(basicVertexShader,  videoFragmentShader);
    renderData->program_tile       = compileProgram(tileVertShader,     tileFragmentShader);
    

    get_uniform(video, alpha);
    get_uniform(video, offset);
    get_uniform(video, y_tex);
    get_uniform(video, u_tex);
    get_uniform(video, v_tex);

    get_uniform(img, alpha);
    get_uniform(img, offset);
    get_uniform(img, tex);

    get_uniform(tile, alpha);
    get_uniform(tile, offset);
    get_uniform(tile, tex);
    get_uniform(tile, vel);

    get_uniform(text, alpha);
    get_uniform(text, offset);
    get_uniform(text, tex);
    get_uniform(text, color);
    get_uniform(text, vel);

    get_uniform(playback, alpha);
    get_uniform(playback, offset);
    get_uniform(playback, vinOffset);

    get_uniform(vin, alpha);
    get_uniform(vin, offset);
    get_uniform(vin, tex);
    get_uniform(vin, vinOffset);
    get_uniform(vin, alphaBlend);
    get_uniform(vin, videoMode);
    

    get_uniform(zoltar, alpha);
    get_uniform(zoltar, brightness);
    get_uniform(zoltar, offset);
    get_uniform(zoltar, tex);
    get_uniform(zoltar, img_ratio);
    get_uniform(zoltar, interest);
    get_uniform(zoltar, slant);
    get_uniform(zoltar, effect);

    get_uniform(highlight, utime);
    get_uniform(highlight, offset);
    get_uniform(highlight, vel);

    mem.free(Vertices);
 
}


