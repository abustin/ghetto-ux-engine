#include "NRD_HAL.h"
#ifdef __ROKU__

#include <stdio.h>
#include <string.h>
#include "roku/RokuAvPlayer.h"
#include "roku/RokuAvBufferQueue.h"
#include "roku/RokuAvTypes.h"


typedef struct {
    RokuAvPlayer *player;
    RokuAvBufferQueue *buffer;
    void (*complete_cb)(void);
    int seqid;
} RokuVideoPlayer;

int digest_frame(unsigned char*** pixels, int** stride, int* flags) {return 0;}

static RokuVideoPlayer* current_video = NULL;
static RokuAvBufferQueue *common_buffer;
static RokuAvPlayer *common_player;

static int playback_state = 0;
static int seqid = 0;

void set_video_window_size(int x,int y,int w,int h) {
    if (current_video) {
        printf("set video window %i %i %i %i\n", x,y,w,h);
        float s = 1.5;
        RokuAvPlayer_SetDstWindow(current_video->player, x*s, y*s, w*s, h*s);
        RokuAvPlayer_ApplyWindowSettings(current_video->player);
    }
}

int get_video_state() {
    return playback_state;
}

static size_t curl_write(void *contents, size_t size, size_t nmemb, void *userp) {

    if (current_video == NULL) {
        return 0;
    }

    RokuVideoPlayer* roku_av = userp;
    if (roku_av->seqid != seqid) {
        return 0;
    }

    int ret;
    size_t len = size * nmemb;
    unsigned char* src = contents;
    unsigned char* dest = NULL;

    ret = RokuAvBufferQueue_GetBuffer(common_buffer, len, true, &dest);
    if (roku_av->seqid != seqid) {
        printf("bail out of put\n");
        return 0;
    }
    if (ret == ROKUAV_OK) {
        memcpy(dest, src, len);
        ret = RokuAvBufferQueue_PutBuffer(common_buffer,len,0,0);
        if (ret != ROKUAV_OK) {
            printf("RokuAvBufferQueue_PutBuffer failed\n");
            return 0;
        }
    } else {
        printf("RokuAvBufferQueue_GetBuffer failed\n");
        return 0;
    }

    return len;
}

static void playback_ended(void *data) {
   playback_state = 0;

}

static  void playback_started(void *cond) {
   printf("playback_started\n");
   playback_state = 2;
}

static  void playback_error(void *cond) {
   printf("playback_error\n");
   playback_state = 0;
}

static  void playback_position(RokuAv_Time position, void* data) {
   //printf("playback_position\n");
   playback_state =2 ;
   printf("position: %ld\n", (long)position);
}

static int curl_prog(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow) {
    //printf("curl_prog %d/%d\n", dlnow,dltotal);
    return (current_video == NULL)? -1 : 0;
}

static int curl_finish(int status, void* data) {
   if (current_video == NULL) return;
   if (current_video->seqid != seqid) {
    printf("bail out of curl_finish\n");
        return 0;
    }
    printf("curl_finish %i\n", status);
    RokuVideoPlayer *roku_av = data;
    RokuAvBufferQueue_SendEndOfStream(common_buffer);
}

void destroy_video() {

    seqid++;
    playback_state = 0;
    if (current_video == NULL) return;
    printf("destroy_video\n");
    RokuAvPlayer_Close(common_player);
    RokuAvBufferQueue_Flush(common_buffer);
    //RokuAvPlayer_Destroy(current_video->player); 
    //RokuAvBufferQueue_Destroy(current_video->buffer);  
    free(current_video);
    current_video = NULL;
}

void stop_video() {
    destroy_video();
}


void play_video(NRDHAL *api, void *url, void (*complete)())  {
    seqid++;

    if (current_video) destroy_video();

    playback_state = 1;

    printf("play_video %s\n", url);

    RokuVideoPlayer *roku_av = malloc(sizeof(RokuVideoPlayer));
    //RokuAvPlayer_Create(&roku_av->player);
    roku_av->buffer =  common_buffer;
    roku_av->player =  common_player;
    roku_av->seqid = seqid;
    
    //RokuAvBufferQueue_CreateWithSize(&roku_av->buffer, 2000);
    //roku_av->complete_cb = complete;

    current_video = roku_av;
    
    RokuAvPlayer_OpenBufferQueue(common_player, ROKU_AV_CFRMT_TS, common_buffer, NULL);
    RokuAvPlayer_Play(roku_av->player);

    api->NRD_http_streamer(url, curl_write, curl_finish, curl_prog, roku_av); 
    
}

void init_streamer(){
    RokuAvPlayer_Create(&common_player);
    RokuAvBufferQueue_CreateWithSize(&common_buffer, 2000);
    
    RokuAvPlayer_SetStreamErrorListener(common_player, playback_error, NULL);
    RokuAvPlayer_SetNewPositionListener(common_player, playback_position, NULL);
    RokuAvPlayer_SetPlaybackStartedListener(common_player, playback_started, NULL);
    RokuAvPlayer_SetPlaybackEndedListener(common_player, playback_ended, NULL);
}



#else

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////



#include <stdio.h>
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "NRD_HAL.h"
#include "render.h"
#include <pthread.h>

static pthread_cond_t  cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

static AVFrame *frame0;
static int seq =0;
static int playing = 0;
static int playback_state = 0;

int get_video_state() {
  return playback_state;
}

int digest_frame(unsigned char*** pixels, int** stride, int* flags) {

    *flags = playing;

    if (frame0) {

        if (pixels != NULL && stride != NULL) {
          
            AVFrame *frame = frame0;
            *pixels = frame->data;
            *stride = frame->linesize;
            playback_state = 2;

        } else {
            frame0 = NULL;
            pthread_cond_signal(&cond);
        }

        return 1;
    }
    return 0;
}

void print_error(const char *filename, int err) {
    char errbuf[128];
    const char *errbuf_ptr = errbuf;
    if (av_strerror(err, errbuf, sizeof(errbuf)) < 0)
        errbuf_ptr = strerror(AVUNERROR(err));
    av_log(NULL, AV_LOG_ERROR, "%s: %s\n", filename, errbuf_ptr);
}

static void init_video_decode(void *url) {

    int ins_seq = seq;

    AVFormatContext* ic = avformat_alloc_context();
    AVCodecContext *avctx;
    AVCodec *codec;

    AVFrame* frame = av_frame_alloc();
    AVPacket avpkt;
    av_init_packet(&avpkt);

    int ret = avformat_open_input (&ic, (char*)url, NULL, NULL);

    if (ret <0) {
        print_error((char*)url, ret);
        return;
    }

    int best_video_stream = av_find_best_stream(ic, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    int best_audio_stream = av_find_best_stream(ic, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);

    //av_dump_format(ic, 0, (char*)url, 0);

    avctx = ic->streams[best_video_stream]->codec;
    codec = avcodec_find_decoder(avctx->codec_id);
    ret = avcodec_open2(avctx, codec, NULL);
    
    av_read_play(ic);
    int c = 0;
    for(;;) {

        if (ins_seq != seq) break;

        int ret = av_read_frame(ic, &avpkt);
        if (ret < 0) {
            if (ret == AVERROR_EOF || (ic->pb && ic->pb->eof_reached))
                break;
            if (ic->pb && ic->pb->error)
                break;
            continue;
        }

        if(avpkt.stream_index == best_video_stream) {

            if (frame0) {
                pthread_mutex_lock(&lock);
                pthread_cond_wait(&cond, &lock);
                pthread_mutex_unlock(&lock);
            }

            ret = 0;
            avcodec_decode_video2(avctx, frame, &ret, &avpkt);
            av_free_packet(&avpkt);
            if (ret) {
                  playing = 1;
                  frame0 = frame;
            }
        }

    }

    
    playback_state = 0;
    frame0 =NULL;
    playing = 0;
    avcodec_close(avctx);
    av_frame_free(&frame);
    avformat_free_context(ic);

}

void stop_video() {
    seq++;
    frame0 =NULL;
    pthread_cond_signal(&cond); 
}

void set_video_window_size(int x,int y,int w,int h){}

void play_video(NRDHAL *api, void *url, void (*complete)()) {
    stop_video();
    playback_state = 1;
    api->NRD_thread(init_video_decode, url);

}

void init_streamer() {
    avcodec_register_all();
    av_register_all();
    avformat_network_init();
}


#endif
