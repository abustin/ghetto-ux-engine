#ifndef _NRD_HAL
#define _NRD_HAL

#include "render_node.h"
#include "glstub.h"
#include <assert.h>
#include <stddef.h>
//#define assert(x)

#ifdef _PORTABLE
//#define NULL 0
//#define M_PI 3.14159265359  
typedef void GLvoid;
typedef int GLsizei;
typedef unsigned short GLushort;
typedef short GLshort;
typedef unsigned char GLubyte;
typedef unsigned int GLenum;
typedef int GLint;
typedef unsigned char GLboolean;
typedef unsigned int GLbitfield;
typedef float GLfloat;
typedef float GLclampf;
typedef signed char GLbyte;
typedef unsigned int GLuint;
typedef int GLfixed;
typedef int GLclampx;
typedef char GLchar;
typedef long GLsizeiptr;

#else

#include "GLES.h"

#endif


typedef struct {
  int quot;
  int rem;
} div_tt;


typedef double (*math_t)(double);
typedef double (*math2_t)(double,double);
typedef div_tt (*div_f) (int numer, int denom);

typedef void (*memset_t)(void *str, int c, int n);
typedef void (*memcpy_t)(void *str1, const void *str2, int n);

struct MemWrapper {
    void * (*malloc)(unsigned int sz);
    void * (*calloc)(unsigned int,unsigned int);
    void (*free)(void *ptr);
    void* (*realloc) (void* ptr, unsigned int size);
    memset_t memset;
    memcpy_t memcpy;
    void*(*printf) ( const char * format, ...);
    char * (*strcat) ( char * destination, const char * source );
    char * (*strcpy) ( char * destination, const char * source );
    char * (*strdup)(const char *s1);
    char * (*strstr)(char* str, const char* target);
    int * (*strncmp) ( const char * str1, const char * str2, unsigned int num );
    int * (*tolower) ( int c );
};

struct MathWrapper {
    div_f div;
    math_t sin;
    math_t cos;
    math_t log;
    math_t round;
    math_t ceil;
    math2_t pow;
    math_t sqrt;
    math2_t atan2;
    math_t asin;
};


typedef void (* glBindTexture_t) (GLenum, GLuint);
typedef void (* glBlendFunc_t) (GLenum, GLenum);
typedef void (* glClear_t) (GLbitfield);
typedef void (* glClearColor_t) (GLclampf, GLclampf, GLclampf, GLclampf);
typedef void (* glDrawArrays_t) (GLenum, GLint, GLsizei);
typedef void (* glGenTextures_t) (GLsizei, GLuint*);
typedef GLenum (* glGetError_t) ();
typedef void (* glTexImage2D_t) (GLenum, GLint, GLint, GLsizei, GLsizei, GLint, GLenum, GLenum, const GLvoid*);
typedef void (* glTexParameteri_t) (GLenum, GLenum, GLint);
typedef void (* glTexSubImage2D_t) (GLenum, GLint, GLint, GLint, GLsizei, GLsizei, GLenum, GLenum, const GLvoid*);
typedef void (* glViewport_t) (GLint, GLint, GLsizei, GLsizei);

typedef void (* glUniform1f_t) (GLint location, GLfloat x);
typedef void (* glUniform1i_t) (GLint location, GLint x);
typedef void (* glUniform4f_t) (GLint location, GLfloat x, GLfloat y, GLfloat z, GLfloat w);
typedef void (* glUniformMatrix4fv_t) (GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);


typedef GLuint (* glCreateShader_t) (GLenum type);
typedef void (* glEnable_t) (GLenum type);
typedef void (* glDisable_t) (GLenum type);
typedef void (*glShaderSource_t) (GLuint shader, GLsizei count, const GLchar** string, const GLint* length);
typedef void (*glCompileShader_t) (GLuint shader);
typedef GLuint (*glCreateProgram_t) (void);
typedef void (*glAttachShader_t) (GLuint program, GLuint shader);
typedef void (*glLinkProgram_t) (GLuint program);
typedef int (*glGetUniformLocation_t) (GLuint program, const GLchar* name);
typedef void  (*glUseProgram_t) (GLuint program);
typedef int (*glGetAttribLocation_t) (GLuint program, const GLchar* name);
typedef void (*glEnableVertexAttribArray_t) (GLuint index);
typedef void (*glGenBuffers_t) (GLsizei n, GLuint* buffers);
typedef void (*glBindBuffer_t) (GLenum target, GLuint buffer);
typedef void (*glBufferData_t) (GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage);
typedef void (*glVertexAttribPointer_t) (GLuint indx, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr);
typedef void (*glActiveTexture_t)(   GLenum texture);
typedef void (*glDeleteTextures_t)(  GLsizei n, const GLuint * textures);
typedef void (*glPixelStorei_t)( GLenum pname, GLint param);

typedef void (* glCullFace_t) (GLenum type);
typedef void (* glFrontFace_t) (GLenum type);

typedef void (*glGetShaderiv_t) (GLuint,GLenum,GLint*);
typedef void (*printOpenGLError_t) (void);
typedef void (*glGetShaderInfoLog_t) (GLuint,GLsizei,GLsizei*,GLchar*);


struct GLWrapper {
    glGetShaderInfoLog_t glGetShaderInfoLog;
    glPixelStorei_t glPixelStorei;
    glDeleteTextures_t glDeleteTextures;
    glActiveTexture_t glActiveTexture;
    glBindTexture_t glBindTexture;
    glBlendFunc_t glBlendFunc;
    glClear_t glClear;
    glClearColor_t glClearColor;
    glDrawArrays_t glDrawArrays;
    glGenTextures_t glGenTextures;
    glGetError_t glGetError;
    glTexImage2D_t glTexImage2D;
    glTexParameteri_t glTexParameteri;
    glTexSubImage2D_t glTexSubImage2D;
    glUniform1f_t glUniform1f;
    glUniform1i_t glUniform1i;
    glUniform4f_t glUniform4f;
    glUniformMatrix4fv_t glUniformMatrix4fv;
    glViewport_t glViewport;
    glCreateShader_t glCreateShader;
    glShaderSource_t glShaderSource;
    glCompileShader_t glCompileShader;
    glCreateProgram_t glCreateProgram;
    glAttachShader_t glAttachShader;
    glUseProgram_t glUseProgram;
    glGetUniformLocation_t glGetUniformLocation;
    glLinkProgram_t glLinkProgram;
    glGetAttribLocation_t glGetAttribLocation;
    glEnableVertexAttribArray_t glEnableVertexAttribArray;
    glGenBuffers_t glGenBuffers;
    glEnable_t glEnable;
    glDisable_t glDisable;
    glBindBuffer_t glBindBuffer;
    glBufferData_t glBufferData;
    glVertexAttribPointer_t glVertexAttribPointer;
    glCullFace_t glCullFace;
    glFrontFace_t glFrontFace;
    glGetShaderiv_t glGetShaderiv;
    printOpenGLError_t printOpenGLError;
};


typedef size_t (*http_write_callback) (char *ptr, size_t size, size_t nmemb, void *userdata);
typedef size_t (*http_finish_callback) (int status, void *userdata);
typedef int (*http_progress_callback) (void *clientp, double dltotal, double dlnow, double ultotal, double ulnow);
typedef void (*network_streamer_t)(char*, http_write_callback, http_finish_callback, http_progress_callback, void*);

extern struct GLWrapper gl;
extern struct MathWrapper math;
extern struct MemWrapper mem;
extern struct NRDHAL *api;

struct NRDThread {
    void *thread;
} typedef NRDThread;

struct NRDHAL {
    void* (*NRD_global_sym)(char *); 
    void (* loop)();
    int (*NRD_rand)();
    void (* NRD_getImageSize)(float *width, float *height);
    void (* NRD_setText)(char* text, int size, int lines, int wrapWidth);
    void (* NRD_setImage)(char *url);
    void (* NRD_drawImage)(struct RenderData *renderData, unsigned int tex, float x, float y, float z, float w, float h);
    NRDThread (* NRD_thread)(void (*thread_main)(void *ptr), void *);
    void (* NRD_thread_join)( NRDThread thread);
    void (* NRD_timeout)(unsigned int usec);
    int (* NRD_pollkey)();
    struct Bitmap *(*NRD_createText)(char* text, int size, int lines, int width);
    struct Bitmap * (*NRD_decodeIMG)(void *mem, int size);
    network_streamer_t NRD_http_streamer; // provided by portable
    int (*NRD_digest_frame)(unsigned char*** pixels, int** stride, int* flags);
    int (*NRD_close_video)();
    int (*NRD_play_video)(char *url);
    int (*NRD_get_video_state)();
    int (*NRD_set_video_window)(int x, int y, int w, int h);
    int (*NRD_get_video_window)(int* x, int* y, int* w, int* h);

} typedef NRDHAL;

void NRD_start_module(NRDHAL *ctx);
void NRD_end_module(NRDHAL *ctx);

#endif
