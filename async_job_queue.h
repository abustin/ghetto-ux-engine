#ifndef _aync_job
#define _aync_job

#define JOB_STACK 300

#define JOB_STATE_EMPTY 0
#define JOB_STATE_PENDING 1
#define JOB_STATE_LOADING 2
#define JOB_STATE_SUCCESS 4
#define JOB_STATE_CANCEL 9

#define JOB_TYPE_DATA 0
#define JOB_TYPE_JSON 1
#define JOB_TYPE_IMG 2
#include <stddef.h>
#include "cJSON.h"
#include "image_decoder.h"
#include "data_loader.h"

struct DataLoadingNode {
    int state;
    int id;
    int retry;
    int cycle;
    int type;
    char *url;
    void *arg;
    void (*onload_data)(void*, struct Bitmap*);
    void (*onload)(void*, cJSON*);
    cJSON* data;
    struct Bitmap *img;
    struct MemoryStruct *buffer;
};

struct MemoryStruct {
    struct Bitmap *image;
    char *memory;
    size_t size;
    size_t memsize;
    int abortWrite;
};

void async_job_queue_boot();
void async_job_queue_shutdown();

void check_waiting();
int curl_json(char *url, void *arg, void (*callback)(void*, cJSON*));
int  curl_data(char *url, void *arg, void (*callback)(void*, struct Bitmap*));
int  curl_data_cancel(int id);
int http_get_complete(struct DataLoadingNode *loader, int cid);
struct DataLoadingNode *get_next_aync_job();
struct DataLoadingNode *get_next_aync_json_job();
#endif
