
#include "texture_loader.h"
#include "NRD_HAL.h"
#include "render_node.h"
#include "text_loader.h"
#include <string.h>

typedef struct {
    TextureUrlNode *head;
    TextureUrlNode *tail;
    unsigned int pixel_count;
    unsigned int pixel_limit;
} TextureLRU;
 

static int total_tex;
static unsigned int highframe;
static struct TextureUrlNode textureStore[BUCKET_SIZE];
static int texture_formats[] = {0, GL_ALPHA, 0, GL_RGB, GL_RGBA};

static float currentWidth;
static float currentHeight;
static int last_tid = 0;

static TextureLRU textureLru = {
    .pixel_limit = 100000
};

#define FNV_PRIME 16777619
#define FNV_OFFSET_BASIS 2166136261

static void foo_destroyTexture(TextureUrlNode *record);

static uint32_t str_hash(char *s) {
    uint32_t hash = FNV_OFFSET_BASIS;
    while (*s != '\0') {
        hash ^= *s;
        hash *= FNV_PRIME;
        s++;
    }
    
    return hash;
};

static void walkLru() {
    TextureUrlNode *node = textureLru.head;
    //printf("start\n");
    while(node) {
        printf("%s lu:%u tx:%u a:%u\n", node->url, node->last_used, node->texture, node->active);
        node = node->lru_next;
    }
    assert(node == textureLru.tail);
    //printf("end\n");
}

unsigned int purgeLru(int w, int h) {

    unsigned int reuse_texture = 0;
    TextureUrlNode *node = textureLru.tail;
    TextureUrlNode *prev_node;
    TextureUrlNode *next_node;

    //printf("purgeLru %u %u %p\n", textureLru.pixel_count , textureLru.pixel_limit, node);
    //walkLru();
    while (!reuse_texture && textureLru.pixel_count > textureLru.pixel_limit && node) {
        prev_node = node->lru_prev;
        next_node = node->lru_next;

        //printf("%i %i\n", node->last_used , highframe);

        if (node->last_used < highframe-2) {
            //printf("purge: %s\n", node->url);
            //printf("%u %u %u\n", node->last_used , highframe, node->texture);

            if (node->width == w && node->height == h) {
                reuse_texture = node->texture;
                node->active = 0;
                node->texture = 0;
            } else {
                foo_destroyTexture(node);
            }
            
        
            if (node->lru_next) node->lru_next->lru_prev = node->lru_prev;
            if (node->lru_prev) node->lru_prev->lru_next = node->lru_next;

            if (node == textureLru.tail) {
                textureLru.tail = prev_node;
                //if (prev_node)
                //prev_node->lru_next = NULL;
            }

            if (node == textureLru.head) {
                textureLru.head = next_node;
            }

            node->lru_prev = NULL;
            node->lru_next = NULL;

        } 
        
        node = prev_node;

        //printf("%i/%i %p\n",textureLru.pixel_count , textureLru.pixel_limit , node);
        
    }

    return reuse_texture;
    //printf("-----\n");
    //walkLru();
    //printf("----done-\n");


}

static void bumpTexture(TextureUrlNode *node) {

    if (textureLru.head) {

        if (textureLru.head != node) {

            if (node->lru_next) node->lru_next->lru_prev = node->lru_prev;
            if (node->lru_prev) node->lru_prev->lru_next = node->lru_next;

            TextureUrlNode *old_head = textureLru.head;

            textureLru.head = node;
            old_head->lru_prev = node;
            node->lru_next = old_head;
            node->lru_prev = NULL;


        }


    } else {
        textureLru.head = node;
        textureLru.tail = node;        
    }

    //purgeLru();
    

}

static struct TextureUrlNode* lookupUrl2(char *url) {

    uint32_t hash = str_hash(url);
    struct TextureUrlNode *node = &textureStore[hash%BUCKET_SIZE];
    while(node) {
        if (node->hash == hash) return node;
        node = node->next;
    }
    return NULL;

}

int should_cancel_texture_load(Loader *loader) {

    struct TextureUrlNode* texture = (struct TextureUrlNode*) loader->context;

    if (texture->last_used < highframe) {
        texture->active = 0;
        return 1;
    } else {
        return 0;
    }
}


// fixme: free subnodes
void purge_texture_cache() {

    struct TextureUrlNode *node = textureStore;
    
    int i = BUCKET_SIZE;

    while (i--) {
        
        struct TextureUrlNode *subnode;

        if (node->texture) {

            mem.printf("del texture: %u\n", node->texture);
            mem.free(node->url);
            gl.glDeleteTextures(1, &node->texture);
        }
        subnode = node->next;
        while (subnode) {
            if (subnode->texture) {
                mem.printf("del texture: %u\n", subnode->texture);
                mem.free(subnode->url);
                gl.glDeleteTextures(1, &subnode->texture);
            }
            subnode = subnode->next;
        }

        node++;
    }


}

struct TextureUrlNode* touchTexture(char *str) {
    uint32_t hash = str_hash(str);
    struct TextureUrlNode *node = &textureStore[hash%BUCKET_SIZE];
    while (node->next && node->hash != hash) {
        node = node->next;
    }
    
    if (node->hash && node->hash != hash) {
        node->next = mem.calloc(1,sizeof(struct TextureUrlNode));
        node = node->next;
    }

    node->hash = hash;
    node->active = 0;
    node->url = str;

    return node;
}


static void foo_destroyTexture(TextureUrlNode *record) {
    //assert(record);
    //printf("foo_destroyTexture %i\n", record->texture);
    if (record->texture) {
        //printf("glDeleteTextures %i\n", record->texture);
        gl.glDeleteTextures(1, &record->texture);
        total_tex--;
        //printf("total_tex %i\n", total_tex);
        record->texture = 0;
        record->active  = 0;
        textureLru.pixel_count -= record->width*record->height;
    }
}

static GLuint createTexture(struct Bitmap *surface, TextureUrlNode *loader) {
    
    //printf("total_tex %i\n", total_tex);
    assert(surface!=NULL);
    assert(surface->pixels !=NULL);
    int texture_format = texture_formats[surface->channels];
    unsigned int reused_tex;


    bumpTexture(loader);
    loader->active = 1;
    loader->width = surface->width;
    loader->height = surface->height;
    textureLru.pixel_count += loader->width*loader->height;


    reused_tex = purgeLru(surface->width, surface->height);

    if (reused_tex) {
        loader->texture = reused_tex;
    } else {
        gl.glGenTextures(1, &loader->texture);        
        total_tex++;
        //printf("total_tex %i %s\n", total_tex, loader->url);
    }


    gl.glActiveTexture(GL_TEXTURE0);
    gl.glBindTexture(GL_TEXTURE_2D, loader->texture);

    //printf("glBindTexture1 %i\n", loader->texture);
    //printf("%i %i %i\n", surface->channels, surface->width, surface->height);
    
    //gl.glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    //gl.glPixelStorei(GL_PACK_ALIGNMENT, 1);
    
    if (surface->width && surface->height) {
        if (reused_tex) {
            gl.glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, surface->width, surface->height, 
                               texture_format, GL_UNSIGNED_BYTE, surface->pixels);
        } else {
            gl.glTexImage2D(GL_TEXTURE_2D, 0, texture_format, surface->width, surface->height, 0, 
                            texture_format, GL_UNSIGNED_BYTE, surface->pixels);
        }
    }

    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);  
    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
    return loader->texture;
}

void setText(struct RenderData* rd,char* text, int size, int lines, int wrapWidth) {

    TextureUrlNode* record = lookupUrl2(text);
    GLuint tid;

    if (!record) {
        record = touchTexture(strdup(text));
    }

    record->last_used = rd->frameCounter;
    highframe = rd->frameCounter;

    if (!record->active) {
        struct Bitmap *img = createText(text, size, lines, wrapWidth);
        createTexture(img, record);
        mem.free(img->pixels);
        img->pixels = NULL;
        mem.free(img);
    }

    tid = record->texture;
    
    last_tid = tid;
    currentWidth = record->width;
    currentHeight = record->height;
    gl.glBindTexture(GL_TEXTURE_2D, tid);
    rd->tex_id = tid;

}



static void curl_loaded(void *l, struct Bitmap* img) {
    Loader *loader = (Loader*) l;
    TextureUrlNode *record = (TextureUrlNode *) loader->context;
    assert(loader);
    assert(record);
    if (img) {
        createTexture(img, record);
        mem.free(img->pixels);
        img->pixels = NULL;
        mem.free(img);
    } else {
        foo_destroyTexture(record);
    }

    if (loader->callback) {
        loader->callback(loader);
    }

    mem.free(loader);
}

void getImageSize(float *width, float *height) {
    *width = currentWidth;
    *height = currentHeight;
}

struct TextureUrlNode* getTexture(char* url) {
    struct TextureUrlNode* record = lookupUrl2(url);
    return (record) ? record : touchTexture(url);
}


static Loader *loadImage(char* url, ImageLoadingHandler callback, void *context, GLuint tex) {

    Loader *loader = calloc(1,sizeof(Loader));
    loader->url = url;
    loader->width = 240;
    loader->height = 240;
    loader->callback = callback;
    loader->context = context;
    loader->load_id = curl_data(url, loader, curl_loaded);  

    return loader;
}


void setTexture(struct RenderData* rd, struct TextureUrlNode* record) {
    GLuint tid = record->texture;
    record->last_used = rd->frameCounter;
    highframe = rd->frameCounter;
    if (tid && last_tid != tid) {
        last_tid = tid;
        currentWidth = record->width;
        currentHeight = record->height;
        gl.glBindTexture(GL_TEXTURE_2D, tid);
        rd->tex_id = tid;
    } else if (!record->active) {
        record->active = 1;
        loadImage(record->url, NULL, record, 0);
        gl.glBindTexture(GL_TEXTURE_2D, 0);
        last_tid = 0;
        rd->tex_id = 0;
    } else {
        gl.glBindTexture(GL_TEXTURE_2D, 0);
        last_tid = 0;
        rd->tex_id = 0;
    }

}








