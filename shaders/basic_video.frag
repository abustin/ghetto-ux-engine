STRINGIFY(

uniform float alpha;

uniform sampler2D y_tex;
uniform sampler2D u_tex;
uniform sampler2D v_tex;

varying vec2 v_texCoord0;

const vec3 R_cf = vec3(1.164383,  0.000000,  1.596027);
const vec3 G_cf = vec3(1.164383, -0.391762, -0.812968);
const vec3 B_cf = vec3(1.164383,  2.017232,  0.000000);
const vec3 offset = vec3(-0.0625, -0.5, -0.5);

void main() {

  vec3 yuv = vec3(
    texture2D(y_tex, v_texCoord0).a,
    texture2D(u_tex, v_texCoord0).a,
    texture2D(v_tex, v_texCoord0).a
  ) + offset;

  gl_FragColor = vec4(
    dot(yuv, R_cf), 
    dot(yuv, G_cf), 
    dot(yuv, B_cf), 
    alpha
  );
  
}

);