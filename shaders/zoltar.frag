STRINGIFY(

varying vec2 v_texCoord0;
varying vec2 v_vertCoord;
varying float v_scale;
uniform sampler2D tex;
uniform float effect;
uniform float alpha;
uniform float brightness;
void main() {
    gl_FragColor = texture2D(tex, v_texCoord0);
    //gl_FragColor = vec4(.0,0.0,0.0,0.0);
    //gl_FragColor.rgb = vec3(.5);
    
    float gradient1 = 0.2 + smoothstep(1.0, 0.3, v_texCoord0.y)*0.8;
    float gradient2 = 0.4 + smoothstep(0.0, 0.1 - (v_scale/.5)*0.09, v_vertCoord.x) *0.6;
    gl_FragColor.rgb *= mix(1.0,gradient1*gradient2,effect);
    

    gl_FragColor.rgb += effect* smoothstep(0.980+(v_scale/0.5)*0.018, 1.0, v_vertCoord.x)*0.1;
    gl_FragColor.rgb *= brightness;
    //gl_FragColor = vec4(0.0,0.0,0.0,0.0);
    gl_FragColor *= alpha;

}


);

