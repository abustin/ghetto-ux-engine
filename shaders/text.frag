STRINGIFY(

varying vec2 v_texCoord0;
uniform sampler2D tex;
uniform float alpha;
uniform vec4 color;

void main() {
    gl_FragColor = color;
    gl_FragColor.a *= texture2D(tex, v_texCoord0).a*alpha;
}

);