STRINGIFY(

varying vec2 v_texCoord0;
uniform sampler2D tex;
uniform float alpha;
varying float screenAlpha;
varying vec2 screenPos;

void main() {
    gl_FragColor = texture2D(tex, v_texCoord0);
    gl_FragColor.a *= screenAlpha*alpha;
    
    gl_FragColor *= 1.0-((smoothstep(-0.45,-0.8, screenPos.y)*smoothstep(0.1,0.80, screenPos.x))*0.8);
    gl_FragColor *= 1.0-(smoothstep(-0.65,-1.0, screenPos.y)*0.3);
    gl_FragColor *= 1.0-(smoothstep(-0.85,-0.98, screenPos.x)*0.6);
    //gl_FragColor *= 1.0-(smoothstep(-0.15,0.15, screenPos.y));
}


);