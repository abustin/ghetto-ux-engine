STRINGIFY(

varying vec2 v_texCoord0;
uniform sampler2D tex;
uniform float alpha;
void main() {
    gl_FragColor = texture2D(tex, v_texCoord0);
    gl_FragColor.a *= alpha;
}


);