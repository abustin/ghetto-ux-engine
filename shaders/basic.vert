STRINGIFY(

attribute vec2 position;
attribute vec2 texCoord0;
uniform vec4 offset;
varying vec2 v_texCoord0;
uniform float vel;


void main(void) {
    v_texCoord0 = texCoord0;
    gl_Position = vec4(offset.zw*position.xy + offset.xy,0.0,1.0);
    //float p = (gl_Position.x)*0.7;
    //gl_Position.y -= p*p;
    float p = (gl_Position.x)*(0.7*vel);
    gl_Position.y -= p*p;
} 

);