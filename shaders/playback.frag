STRINGIFY(

uniform float alpha;
uniform float vinOffset;
varying vec2 v_texCoord0;

void main() {

    float fade = alpha
                 * smoothstep(-0.1+vinOffset, 0.4+(vinOffset*4.0), -0.35+v_texCoord0.x+ ((v_texCoord0.y)*1.0)*vinOffset + v_texCoord0.y*0.1);
    
    vec4 vid = vec4(0.0);
    vid.a = 1.0 - fade; 
    vid.a *= alpha;
    
    gl_FragColor = vid;


}

);