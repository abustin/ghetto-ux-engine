STRINGIFY(

uniform float utime;
varying vec2 v_texCoord0;

const float M_PI = 3.1415926535897932384626433832795;

void main() {

    float border = 0.015;
    float bordery = border*1.7;

    gl_FragColor = vec4(0.0);
    gl_FragColor.a = 1.0;
    float wave = -(v_texCoord0.x*2.0)+utime*2.0;

    float distance_from_edge = min(sin(v_texCoord0.x*M_PI)  , sin(v_texCoord0.y*M_PI));
    distance_from_edge = smoothstep(0.3, 0.0, distance_from_edge);

    gl_FragColor.a  = distance_from_edge*distance_from_edge*0.33;

    if (v_texCoord0.x < border ||  v_texCoord0.x > 1.0-border || v_texCoord0.y < bordery || v_texCoord0.y > 1.0-bordery) {
       gl_FragColor.rgba = vec4(1.0-sin(wave));
       gl_FragColor.rgb *= 0.66;
    }
}


);