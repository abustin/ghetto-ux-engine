STRINGIFY(

attribute vec2 position;
attribute vec2 texCoord0;
uniform vec4 offset;
uniform float img_ratio;
uniform float interest;
varying vec2 v_texCoord0;
uniform float slant;
varying vec2 v_vertCoord;
varying float v_scale;

void main(void) {

    float scale_ratio = offset.z/offset.w;
    float scale = scale_ratio/img_ratio;

    v_texCoord0 = texCoord0;
    v_texCoord0.x *= scale_ratio;
    v_texCoord0.x += .5 - scale_ratio*.5;
    v_texCoord0.x += interest * (1.0-scale);

    v_scale = scale;

    gl_Position = vec4(offset.zw*position.xy + offset.xy,0.0,1.0);
    gl_Position.x += (gl_Position.y*0.1) * slant;
    v_texCoord0.x += (v_texCoord0.y*-0.1) * slant;
    v_vertCoord = position;
} 

);


/*
void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float rat1 = 1.5;
    float rat2 = 0.4 + 0.5 + sin(iGlobalTime)*0.5;
    float scale = rat2/rat1;
    float scale2 = rat1/rat2;
    float inter = sin(iGlobalTime*0.321)*.5;
    vec2 uv = fragCoord.xy / iResolution.xy;
   
    uv.x *= scale;
    uv.x += .5 - scale/2.0;
    uv.x += inter * (1.0-scale);
    
    uv.y *= -1.0;
    fragColor = texture2D(iChannel0, uv);
}
*/