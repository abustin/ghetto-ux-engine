STRINGIFY(

attribute vec2 position;
attribute vec2 texCoord0;
uniform vec4 offset;
varying vec2 v_texCoord0;
varying float screenAlpha;
uniform float vel;
varying vec2 screenPos;


void main(void) {
    v_texCoord0 = texCoord0;
    gl_Position = vec4(offset.zw*position.xy + offset.xy,0.0,1.0);
    screenAlpha = 1.0;
    screenPos = vec2(gl_Position.xy);

    float p = (gl_Position.x)*(0.7*vel);
    gl_Position.y -= p*p;
} 

);