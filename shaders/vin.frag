STRINGIFY(

varying vec2 v_texCoord0;
uniform sampler2D tex;
uniform float alpha;
uniform float vinOffset;
uniform float videoMode;
uniform bool alphaBlend;

void main() {
    vec4 image = texture2D(tex, v_texCoord0);

    float fade = alpha
                 * smoothstep(1.0, 0.7, v_texCoord0.y)
                 * smoothstep(-0.1+vinOffset, 0.3+(vinOffset*4.0), -0.15+v_texCoord0.x+ ((v_texCoord0.y)*1.0)*vinOffset + v_texCoord0.y*0.1);

    if (alphaBlend) {
        image *= -1.0 * fade*(fade-2.0);
    } else {
        image.rgb *= fade;
    }

    
    vec4 vid = vec4(0.0);
    vid.a = 1.0 - fade; 

    gl_FragColor = mix(image,vid,videoMode*videoMode*videoMode); 




}

);