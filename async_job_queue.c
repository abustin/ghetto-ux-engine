#include "async_job_queue.h"
#include "NRD_HAL.h"

static struct DataLoadingNode loading_stack[JOB_STACK];

static int loopCount =0;

static int maxJobs = 0;

void async_job_queue_boot() {
    data_loader_boot();
}

void async_job_queue_shutdown() {
    data_loader_shutdown();
}

void check_waiting(int (*should_cancel)(void*)) {

    int emptyJobs = 0;
    loopCount++;

    int i;
    struct DataLoadingNode *loader;
    int had_success = 0;

    for (i=0;i<maxJobs;i++) {

        loader = &loading_stack[i];

        assert(loader);

        switch (loader->state) {
            case JOB_STATE_PENDING:
            case JOB_STATE_LOADING:

                if (loader->type == JOB_TYPE_DATA)
                if (should_cancel(loader->arg)) {
                    ++loader->cycle;
                    loader->state = JOB_STATE_CANCEL;
                }

                break;
            case JOB_STATE_CANCEL:
                ++loader->cycle;
                loader->data = NULL;
                if (loader->url) {
                    mem.free(loader->url);   
                    loader->url = NULL;
                }
                if (loader->img) {
                    if (loader->img->pixels) {
                        mem.free(loader->img->pixels);
                        loader->img->pixels = NULL;
                    }
                    mem.free(loader->img);
                    loader->img = NULL;
                }

                if (loader->type == JOB_TYPE_DATA) {
                    if (loader->onload_data) {
                        loader->onload_data(loader->arg, NULL);
                        
                    }
                }

                else if (loader->type == JOB_TYPE_JSON) {
                    if (loader->onload) {
                        loader->onload(loader->arg, NULL);
                        loader->onload = NULL;
                    }
                }

                loader->onload_data = NULL;
                loader->onload = NULL;
                loader->arg = NULL;
                loader->state = JOB_STATE_EMPTY;
                break;

            case JOB_STATE_SUCCESS:

                if (had_success) break;

                if (loader->type == JOB_TYPE_DATA) {
                    if (loader->onload_data) {
                        loader->onload_data(loader->arg, loader->img);
                        loader->img = NULL;
                    }
                }

                    
                else if (loader->type == JOB_TYPE_JSON) {
                    if (loader->onload) {
                        loader->onload(loader->arg, loader->data);
                        loader->data = NULL;
                    }
                } 
                ++loader->cycle;
                loader->data = NULL;
                loader->arg = NULL;
                loader->onload = NULL;
                loader->onload_data = NULL;
                if (loader->url) {
                    mem.free(loader->url);
                    loader->url = NULL;
                }
                
                loader->state = JOB_STATE_EMPTY;
                had_success = 1;
                break;

            case JOB_STATE_EMPTY:
                emptyJobs++;
                break;


        }

    }

    if (maxJobs > 0 && loader->state == JOB_STATE_EMPTY) {
        maxJobs--;
    } 

    if ((loopCount%100) == 0) {
        //printf("Empty Jobs: %i  MaxJobs:%i \n", emptyJobs, maxJobs);
    }

}


struct DataLoadingNode *get_next_aync_json_job() {

    int i;
    for (i=0;i<maxJobs;i++) {
        struct DataLoadingNode *loader = &loading_stack[i];
        if (loader->state == JOB_STATE_PENDING && loader->type == JOB_TYPE_JSON) {
            loader->state = JOB_STATE_LOADING;
            return loader;
        }
    }

    return NULL;

}

struct DataLoadingNode *get_next_aync_job() {

    int i;
    for (i=0;i<maxJobs;i++) {
        struct DataLoadingNode *loader = &loading_stack[i];
        if (loader->state == JOB_STATE_PENDING) {
            loader->state = JOB_STATE_LOADING;
            return loader;
        }
    }

    return NULL;

}

static unsigned short job_id_count = 1;

static struct DataLoadingNode *add_to_loading(char *url,int type,void *arg) {
    int i;
    for (i=0;i<JOB_STACK;i++) {

        struct DataLoadingNode *loader = &loading_stack[i];
        if (loader->state == JOB_STATE_EMPTY) {
            loader->id = (i<<16) | job_id_count;
            job_id_count++;
            loader->type = type;
            loader->arg = arg;
            loader->url = mem.strdup(url);
            loader->state = JOB_STATE_PENDING;
            i++;
            if (i>maxJobs) {
                maxJobs = i;
            }
            return loader;
        }

    }
    return NULL;
}


int curl_data_cancel(int i){

    int idx = (i>>16);

    struct DataLoadingNode *loader = &loading_stack[idx];
    //printf("%i %i %s\n",  loader->id, i, loader->url);
    if (loader && loader->state != JOB_STATE_EMPTY && loader->id == i) {
        ++loader->cycle;
        //printf("cancel!\n");
        loader->state = JOB_STATE_CANCEL;
        return 1;
    }
    return 0;
}

int curl_data(char *url, void *arg, void (*callback)(void *arg, struct Bitmap*)) {
    struct DataLoadingNode *loader = add_to_loading(url, JOB_TYPE_DATA, arg);
    assert(loader);
    assert(callback);
    loader->onload_data = callback;
    wake_loader();
    return loader->id;
}

int curl_img(char *url, void *arg, void (*callback)(void *arg, struct Bitmap*)) {
    struct DataLoadingNode *loader = add_to_loading(url, JOB_TYPE_IMG, arg);
    assert(loader);
    assert(callback);
    loader->onload_data = callback;
    wake_loader();
    return loader->id;
}




int curl_json(char *url, void *arg, void (*callback)(void*, cJSON*))  {

    
    struct DataLoadingNode *loader = add_to_loading(url, JOB_TYPE_JSON, arg);
    assert(loader);
    loader->onload = callback;
    wake_loader();
    return loader->id;

    
}


int http_get_complete(struct DataLoadingNode *loader, int cid) {
    if (loader->cycle != cid) {
        return 0;
    }

    if (loader->type == JOB_TYPE_DATA) {
        loader->data = NULL;

        if (loader->cycle == cid) {
           loader->img = api->NRD_decodeIMG(loader->buffer->memory, loader->buffer->size);
        }
    }

    if (loader->type == JOB_TYPE_JSON) {
        cJSON *json = NULL;

        //printf("out: %.50s (end)\n", loader->buffer->memory);
        json =cJSON_Parse(loader->buffer->memory);

        if (loader->cycle == cid) {
            loader->data = json;
        }
    }

    if (loader->cycle == cid) {
        loader->state = JOB_STATE_SUCCESS;
        return 1;
    } else {
        return 0;
    }
}
