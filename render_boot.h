#ifndef render_boot_h
#define render_boot_h

void setupWindow(int w, int h);
void finishFrame();
int get_last_key();

#endif

