#include "render_node.h"
#include "render.h"
#include "texture_loader.h"
#include <math.h>

typedef struct {
    TextureUrlNode *texture;
    float start, end, y;
} Layer;

static Layer layers[4];
static int init_done = 0;

static void init_screensaver() {
    
    layers[0].texture = getTexture("http://cdn-0.nflximg.com/us/ffe/htmltvui/darwin/screensaver/adult/house-1.jpg");
    layers[1].texture = getTexture("http://cdn-0.nflximg.com/us/ffe/htmltvui/darwin/screensaver/adult/house-2.png");
    layers[2].texture = getTexture("http://cdn-0.nflximg.com/us/ffe/htmltvui/darwin/screensaver/adult/house-3.png");
    layers[3].texture = getTexture("http://cdn-0.nflximg.com/us/ffe/htmltvui/darwin/screensaver/adult/house-4.png");

    layers[0].start = -1;
    layers[0].end = -72;

    layers[1].start = 0;
    layers[1].end = -127;
    layers[1].y = 2;

    layers[2].start = 704;
    layers[2].end = 515;
    layers[2].y = 3;

    layers[3].start = 140;
    layers[3].end = 90;
    layers[3].y = 506;

}

void draw_screensaver(struct RenderData *ctx) {

    ctx->alpha = 1;
    
    if (!init_done) init_screensaver();
    int i;

    float p = fmod(ctx->time , 30) / 30;

    for (i =0; i<4; i++) {

        float x = layers[i].start;

        x += (layers[i].end - layers[i].start)*p;

        setTexture(ctx, layers[i].texture);
        drawImage(ctx, 0, x, layers[i].y, 0);

    }

}

