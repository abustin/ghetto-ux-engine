#include "NRD_HAL.h"
#include "render.h"
#include "texture_loader.h"

#ifndef HAVE_BUILTIN_SINCOS
#define sincos _sincos
static void sincos (double a, double *s, double *c){
    *s = math.sin (a);
    *c = math.cos (a);
}
#endif

#define check()
#define GL_R8 GL_ALPHA
#define GL_RED GL_ALPHA
#define WIDTH 1280.0
#define HEIGHT 720.0

static int vid_w = 1280;
static int vid_h = 720;

static unsigned int y_tex;
static unsigned int u_tex;
static unsigned int v_tex;

static RenderData renderData;

static void identity(float *m) {
    m[0] = 1.0;
    m[1] = 0.0;
    m[2] = 0.0;
    m[3] = 0.0;
    m[4] = 0.0;
    m[5] = 1.0;
    m[6] = 0.0;
    m[7] = 0.0;
    m[8] = 0.0;
    m[9] = 0.0;
    m[10] = 1.0;
    m[11] = 0.0;
    m[12] = 0.0;
    m[13] = 0.0;
    m[14] = 0.0;
    m[15] = 1.0;
}

static void scale(float *m, float x, float y, float z) {
    m[0] *= x;
    m[1] *= x;
    m[2] *= x;
    m[3] *= x;
    m[4] *= y;
    m[5] *= y;
    m[6] *= y;
    m[7] *= y;
    m[8] *= z;
    m[9] *= z;
    m[10] *= z;
    m[11] *= z;
}


static void transpose(float *m) {
    float t[16] = {
        m[0], m[4], m[8],  m[12],
        m[1], m[5], m[9],  m[13],
        m[2], m[6], m[10], m[14],
        m[3], m[7], m[11], m[15]};

    mem.memcpy(m, t, sizeof(t));
}


static void multiply(float *m, float *n) {
    float tmp[16];
    const float *row, *column;
    div_tt d;
    int i, j;
 
    for (i = 0; i < 16; i++) {
       tmp[i] = 0;
       d = math.div(i, 4);
       row = n + d.quot * 4;
       column = m + d.rem;
       for (j = 0; j < 4; j++)
          tmp[i] += row[j] * column[j * 4];
    }
    mem.memcpy(m, tmp, sizeof(tmp));
}

static void translate(float *m, float x, float y, float z) {
    float t[16] = { 1.0, 0.0, 0.0, 0.0,  0.0, 1.0, 0.0, 0.0,  0.0, 0.0, 1.0, 0.0,  x, y, z, 1.0 };
    multiply(m, t);
}


static void rotationX(float *m, float Theta)
{
    float CosT = math.cos(Theta);
    float SinT = math.sin(Theta);

    identity(m);

    m[4+1] = CosT;
    m[4+2] = SinT;
    m[9+1] = -SinT;
    m[9+2] = CosT;
}

static void rotationY(float *m, float Theta)
{
    float CosT = math.cos(Theta);
    float SinT = math.sin(Theta);

    identity(m);
    m[0+0] = CosT;
    m[0+2] = SinT;
    m[9+0] = -SinT;
    m[9+2] = CosT;
}

static void rotationZ(float *m, float Theta)
{
    float CosT = math.cos(Theta);
    float SinT = math.sin(Theta);

    identity(m);

    m[0+0] = CosT;
    m[0+1] = SinT;
    m[4+0] = -SinT;
    m[4+1] = CosT;
}

__inline static void translate2(float *a, float x, float y, float z) {
    a[12] = x;
    a[13] = y;
    a[14] = z;
    a[15] = 1.0;
}


static void otho(float *m, float zNear, float zFar) {
    
    float tmp[16];
    identity(tmp);

    tmp[0] = 2.0/WIDTH;
 
    tmp[5] = 2.0/HEIGHT;
    tmp[10]= 1 / (zFar - zNear);
    tmp[11]= -zNear / (zFar - zNear);

    tmp[12]= -1.0;
    tmp[13]= 1.0;

    mem.memcpy(m, tmp, sizeof(tmp));
}

static void perspective(float *m, float fovy, float aspect, float zNear, float zFar)
{
    float tmp[16];
    identity(tmp);
 
    double sine, cosine, cotangent, deltaZ;
    float radians = fovy / 2 * M_PI / 180;
 
    deltaZ = zFar - zNear;
    sincos(radians, &sine, &cosine);
 
    if ((deltaZ == 0) || (sine == 0) || (aspect == 0))
       return;
 
    cotangent = cosine / sine;
 
    tmp[0] = cotangent / aspect;
    tmp[5] = cotangent;
    tmp[10] = -(zFar + zNear) / deltaZ;
    tmp[11] = -1;
    tmp[14] = -2 * zNear * zFar / deltaZ;
    tmp[15] = 0;
 
    mem.memcpy(m, tmp, sizeof(tmp));
}


void drawColoredImage(struct RenderData *rd, GLuint tex, float x, float y, float z, float r, float g, float b, float a) {
    rd->drawState = 1;
    gl.glUseProgram(rd->program_text);

    
    gl.glUniform4f(rd->program_text_color, r,g,b,a);
    drawImage(rd, tex, x,y,z);
    rd->drawState = 0;
}

void drawHighlight(struct RenderData *rd, float x, float y, float w, float h) {

    gl.glUseProgram(rd->program_highlight);
    gl.glUniform1f(rd->program_highlight_utime,  rd->time);
    gl.glUniform1f(rd->program_highlight_vel,  rd->demo0);
    gl.glUniform4f(rd->program_highlight_offset, -1+x/640,1+((y/360)*-1),w/640,h/360);

    gl.glDrawArrays(GL_TRIANGLE_STRIP, 0, rd->num_verts);

}


void drawZoltarImage(struct RenderData *rd, float x, float y, float w, float h, float interest, float slant, float effect, float bright) {

    if (rd->alpha < 0.001 && !slant) return;

    float iw =0; 
    float ih =0;
    getImageSize(&iw, &ih);

    gl.glUseProgram(rd->program_zoltar);
    gl.glUniform1f(rd->program_zoltar_brightness, bright);
    gl.glUniform1f(rd->program_zoltar_alpha, rd->alpha);
    gl.glUniform1i(rd->program_zoltar_tex, 0);
    gl.glUniform1f(rd->program_zoltar_img_ratio, iw/ih);
    gl.glUniform1f(rd->program_zoltar_interest, interest);
    gl.glUniform1f(rd->program_zoltar_slant, slant);
    gl.glUniform1f(rd->program_zoltar_effect, effect);
    gl.glUniform4f(rd->program_zoltar_offset,-1+x/640,1+((y/360)*-1),w/640,h/360);
    gl.glDisable(GL_BLEND);
    gl.glDrawArrays(GL_TRIANGLE_STRIP, 0, rd->num_verts);
    gl.glEnable(GL_BLEND);
}

void drawGalleryTile(struct RenderData *rd, unsigned int tex, float x, float y, float z, float w, float h, float v) {

    if (rd->alpha < 0.001) return;

    gl.glUseProgram(rd->program_tile);
    gl.glUniform1i(rd->program_tile_tex, 0);

    gl.glUniform1f(rd->program_tile_vel, rd->demo0);
    gl.glUniform1f(rd->program_tile_alpha, rd->alpha);
    gl.glUniform4f(rd->program_tile_offset,-1+x/640,1+((y/360)*-1),w/640,h/360);

    gl.glDrawArrays(GL_TRIANGLE_STRIP, 0, rd->num_verts);

}

void drawImage(struct RenderData *rd, GLuint tex, float x, float y, float z) {
    float w =0; 
    float h =0;
    getImageSize(&w, &h);

    if (rd->drawState) {
        if (h < 60) {
        gl.glUniform1f(rd->program_text_vel, rd->demo0);
        } else {
            gl.glUniform1f(rd->program_text_vel, 0);
        } 
    }

    drawSizedImage(rd, tex, x,y,z,w,h);    
}

void setYPixels(unsigned char* pixels, int stride) {
  gl.glBindTexture(GL_TEXTURE_2D, y_tex);
  gl.glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, vid_w, vid_h, GL_RED, GL_UNSIGNED_BYTE, pixels);
}
 
void setUPixels(unsigned char* pixels, int stride) {
  gl.glBindTexture(GL_TEXTURE_2D, u_tex);
  gl.glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, vid_w/2, vid_h/2, GL_RED, GL_UNSIGNED_BYTE, pixels);
}
 
void setVPixels(unsigned char* pixels, int stride) {
  gl.glBindTexture(GL_TEXTURE_2D, v_tex);
  gl.glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, vid_w/2, vid_h/2, GL_RED, GL_UNSIGNED_BYTE, pixels);
}

static int show_frame = 0;

void drawVideo(struct RenderData *rd) {

    int x,y,w,h;

    api->NRD_get_video_window(&x,&y,&w,&h);

    unsigned char** pixels;
    int* stride;
    int flags;

    // 24 fps hack ... meh
    if (rd->frameCounter % 2 == 0 && rd->frameCounter % 5 != 0) {
        if (api->NRD_digest_frame(&pixels, &stride, &flags)) {
            setYPixels(pixels[0], stride[0]);
            setUPixels(pixels[1], stride[1]);
            setVPixels(pixels[2], stride[2]);
        }

        api->NRD_digest_frame(NULL,NULL, &flags);
        show_frame=  flags;
    }


    if (!show_frame) return;

    gl.glUseProgram(rd->program_video);

    gl.glActiveTexture(GL_TEXTURE1);
    gl.glBindTexture(GL_TEXTURE_2D, u_tex);

    gl.glActiveTexture(GL_TEXTURE2);
    gl.glBindTexture(GL_TEXTURE_2D, v_tex);

    gl.glActiveTexture(GL_TEXTURE0);
    gl.glBindTexture(GL_TEXTURE_2D, y_tex);


    gl.glUniform1f(rd->program_video_alpha,1);
    gl.glUniform4f(rd->program_video_offset,-1+(float)x/640,1+(((float)y/360)*-1),(float)w/640,(float)h/360);
    gl.glUniform1i(rd->program_video_y_tex, 0);
    gl.glUniform1i(rd->program_video_u_tex, 1);
    gl.glUniform1i(rd->program_video_v_tex, 2);

    gl.glDrawArrays(GL_TRIANGLE_STRIP, 0, rd->num_verts);

}

void drawVideoPlane(struct RenderData *rd, float x, float y, float w, float h, float wipe) {

    gl.glUseProgram(rd->program_playback);
    gl.glUniform1f(rd->program_playback_vinOffset, wipe);
    gl.glUniform1f(rd->program_playback_alpha, rd->alpha);
    gl.glUniform4f(rd->program_playback_offset,-1+x/640,1+((y/360)*-1),w/640,h/360);

    gl.glDisable(GL_BLEND);
    gl.glDrawArrays(GL_TRIANGLE_STRIP, 0, rd->num_verts);
    gl.glEnable(GL_BLEND);
    
}

void drawVin(struct RenderData *rd, float x, float y, float w, float h, float wipe, float videoMode, int blend) {

    gl.glUseProgram(rd->program_vin);
    gl.glUniform1f(rd->program_vin_vinOffset, wipe);
    gl.glUniform1f(rd->program_vin_alpha, rd->alpha);
    gl.glUniform1f(rd->program_vin_videoMode, videoMode);
    gl.glUniform1i(rd->program_vin_tex, 0);
    gl.glUniform1i(rd->program_vin_alphaBlend, blend);
    gl.glUniform4f(rd->program_vin_offset,-1+x/640,1+((y/360)*-1),w/640,h/360);

    if (!blend) gl.glDisable(GL_BLEND);
    gl.glDrawArrays(GL_TRIANGLE_STRIP, 0, rd->num_verts);
    if (!blend) gl.glEnable(GL_BLEND);
    
}


void drawSizedImage(struct RenderData *rd, unsigned int tex, float x, float y, float z, float w, float h) {
    
    if (rd->alpha < 0.001 || rd->tex_id ==0 ) return;

    if (rd->drawState) {

        gl.glUniform1f(rd->program_text_alpha, rd->alpha);
        gl.glUniform1i(rd->program_text_tex, 0);
        gl.glUniform4f(rd->program_text_offset,-1+x/640,1+((y/360)*-1),w/640,h/360);
    } else {
        gl.glUseProgram(rd->program_img);
        gl.glUniform1i(rd->program_img_tex, 0);
        gl.glUniform1f(rd->program_img_alpha, rd->alpha);
        gl.glUniform4f(rd->program_img_offset,-1+x/640,1+((y/360)*-1),w/640,h/360);
    }

    gl.glDrawArrays(GL_TRIANGLE_STRIP, 0, rd->num_verts);
    
}

static void initVideo() {
    

    gl.glGenTextures(1, &y_tex);
    gl.glBindTexture(GL_TEXTURE_2D, y_tex);
    gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, vid_w, vid_h, 0, GL_RED, GL_UNSIGNED_BYTE, NULL); 
    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    gl.glGenTextures(1, &u_tex);
    gl.glBindTexture(GL_TEXTURE_2D, u_tex);
    gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, vid_w/2, vid_h/2, 0, GL_RED, GL_UNSIGNED_BYTE, NULL);
    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    gl.glGenTextures(1, &v_tex);
    gl.glBindTexture(GL_TEXTURE_2D, v_tex);
    gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, vid_w/2, vid_h/2, 0, GL_RED, GL_UNSIGNED_BYTE, NULL);
    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

}

void loop(void (*innerLoop)(struct RenderData*)) {
    
    renderData.frameCounter++;

    // Dont have real timers setup yet
    #ifdef __ROKU__
    renderData.time += 0.016*2; // 30fps
    #else 
    renderData.time += 0.016; // 60 fps
    #endif

    gl.glClear(GL_COLOR_BUFFER_BIT);
    innerLoop(&renderData);

    #ifndef __ROKU__
    gl.glBlendFunc (GL_ONE_MINUS_DST_ALPHA, GL_SRC_ALPHA);
    drawVideo(&renderData);
    gl.glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    #endif

}

void cleanGL() {
    // fixme - delete buffers and programs
    purge_texture_cache();
}

void initGL() {

    mem.printf("make program\n");
    makeProgram(&renderData);
    mem.printf("init initProgram\n");
    
    gl.glClearColor( 0.0, 0.0, 0.0, 1.0 );
    gl.glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    gl.glViewport(0, 0, WIDTH, HEIGHT);
    gl.glEnable(GL_BLEND);
    
    #ifndef __ROKU__
    initVideo();
    #endif
    
}


