#ifndef _render_node_h_
#define _render_node_h_

#define draw_func void (*draw)(struct RenderData *ctx, void *node)

typedef struct RenderData {

    float time;
    unsigned int tex_id;
    
    unsigned int program_highlight;
    unsigned int program_highlight_offset;
    unsigned int program_highlight_utime;
    unsigned int program_highlight_vel;
    
    unsigned int program_img;
    unsigned int program_img_alpha;
    unsigned int program_img_offset;
    unsigned int program_img_tex;
    
    unsigned int program_text;
    unsigned int program_text_alpha;
    unsigned int program_text_color;
    unsigned int program_text_offset;
    unsigned int program_text_tex;
    unsigned int program_text_vel;
    
    unsigned int program_tile;
    unsigned int program_tile_alpha;
    unsigned int program_tile_offset;
    unsigned int program_tile_tex;
    unsigned int program_tile_vel;
    
    unsigned int program_video;
    unsigned int program_video_alpha;
    unsigned int program_video_offset;
    unsigned int program_video_u_tex;
    unsigned int program_video_v_tex;
    unsigned int program_video_y_tex;
    
    unsigned int program_vin;
    unsigned int program_vin_alpha;
    unsigned int program_vin_alphaBlend;
    unsigned int program_vin_offset;
    unsigned int program_vin_tex;
    unsigned int program_vin_videoMode;
    unsigned int program_vin_vinOffset;
    
    unsigned int program_zoltar;
    unsigned int program_zoltar_alpha;
    unsigned int program_zoltar_brightness;
    unsigned int program_zoltar_effect;
    unsigned int program_zoltar_img_ratio;
    unsigned int program_zoltar_interest;
    unsigned int program_zoltar_offset;
    unsigned int program_zoltar_slant;
    unsigned int program_zoltar_tex;

    unsigned int program_playback;
    unsigned int program_playback_alpha;
    unsigned int program_playback_offset;
    unsigned int program_playback_vinOffset;

    unsigned int num_verts;
    unsigned int frameCounter;
    unsigned int useVin;
    float vinOffset;
    float alpha;
    float demo0;
    char drawState;
} RenderData;

#endif
