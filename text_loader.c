#include <ft2build.h>
#include FT_FREETYPE_H
#include "text_loader.h"
#include "assert.h"


#define FONT_SIZE 20

static int nextpoweroftwo(int x) {
    return x + 4 - x%4;
    //double logbase2 = log(x) / log(2);
    //return round(pow(2,ceil(logbase2)));
}

static uint16_t tmpUnicodeStr[1000];

static uint16_t *make_unicode_string(char *utf8) {
    int size = 0, index = 0, out_index = 0;
    uint16_t *out;
    unsigned char c;

    /* first calculate the size of the target string */
    c = utf8[index++];
    while(c) {
        if((c & 0x80) == 0) {
            index += 0;
        } else if((c & 0xe0) == 0xe0) {
            index += 2;
        } else {
            index += 1;
        }
        size += 1;
        c = utf8[index++];
    }   

    out = &tmpUnicodeStr[0];
    
    index = 0;

    c = utf8[index++];
    while(c)
    {
        if((c & 0x80) == 0) {
            out[out_index++] = c;
        } else if((c & 0xe0) == 0xe0) {
            out[out_index] = (c & 0x1F) << 12;
            c = utf8[index++];
            out[out_index] |= (c & 0x3F) << 6;
            c = utf8[index++];
            out[out_index++] |= (c & 0x3F);
        } else {
            out[out_index] = (c & 0x3F) << 6;
            c = utf8[index++];
            out[out_index++] |= (c & 0x3F);
        }
        c = utf8[index++];
    }
    out[out_index] = 0;
    assert(out_index<1000);

    return out;
}

static FT_Library library;
static FT_Face face;
static int lastSize = 24;


void *getFontBuffer();
int64_t getFontSize();
void initText() {
    printf("initText\n");
    
    FT_Init_FreeType(&library);
    int error;

    char fontFile[PATH_MAX + 1];
    #ifdef __ROKU__
    FT_New_Face(library, "pkg_/Arial_for_Netflix-B.ttf", 0, &face);
    #else 
    realpath("./Arial_for_Netflix-B.ttf", fontFile);
    printf("%s\n", fontFile);
    FT_New_Face(library, fontFile, 0, &face);
    //FT_New_Face(library, "@executable_path/Arial_for_Netflix-B.ttf", 0, &face);
    #endif
    
    printf("font face %p\n", face);
    
    FT_Set_Pixel_Sizes(face, 0, lastSize);
}

static void draw_bitmap(FT_GlyphSlot slot, FT_Vector pen, struct Bitmap *bitmap) {

    int  i, j, p, q;
    
    int x_max = slot->bitmap.width;
    int y_max = slot->bitmap.rows;
  
    int x = pen.x + slot->bitmap_left;
    int y = pen.y - slot->bitmap_top;

    int p1 = 0;
    int q1 = 0;

    int dest_width = bitmap->width;
    int dest_height = bitmap->height;
    int src_width = slot->bitmap.width;

    unsigned char *dest = bitmap->pixels;
    unsigned char *src = slot->bitmap.buffer;

    if (x<0) {
        p1 -= x;
        x = 0;
    }
    if (y<0) {
        q1 -= y;
        y = 0;
    }

    for (i=x, p = p1; p < x_max; i++, p++) {
        for (j = y, q = q1; q < y_max; j++, q++) {
            if (i >= dest_width || j >= dest_height) continue;
            dest[dest_width*j + i] |= src[q * src_width + p];
        }
    }
}

struct Bitmap *createText(char* text, int size, int lines, int width) {

    if (size != lastSize) {
        lastSize = size;
        FT_Set_Pixel_Sizes(face, 0, size);
    }
    
    FT_GlyphSlot  slot;
    FT_Matrix     matrix;                 /* transformation matrix */
    FT_Vector     pen;                    /* untransformed origin  */
    FT_Error      error;
  
    double        angle;
    int           target_height;
    int           n, num_chars;
  
    num_chars     = strlen(text);
    target_height = size + (int)(size*0.25);

    int displayHeight;
    int displayWidth;

    if (lines >1) {
        displayWidth = nextpoweroftwo(width);
        displayHeight = nextpoweroftwo(target_height*lines);
    } else {
        displayHeight = nextpoweroftwo(target_height);
        displayWidth = nextpoweroftwo(num_chars*size*0.65);
    }
    
    int pixels = displayWidth*displayHeight;

    struct Bitmap *bitmap = malloc(sizeof(struct Bitmap));
    bitmap->pitch = displayWidth;
    bitmap->width = displayWidth;
    bitmap->height = displayHeight;
    bitmap->channels = 1;
    
    bitmap->pixels = calloc(pixels,sizeof(char));

    FT_UInt       glyph_index=0;
    FT_Bool       use_kerning=0;
    FT_UInt       previous=0;

    slot = face->glyph;
  
    pen.x = 0;
    pen.y = size;
    
    uint16_t *foo = make_unicode_string(text);
    
    use_kerning = FT_HAS_KERNING( face );
    
    for (n = 0; n < num_chars; n++) {

        uint16_t ucode = foo[n];

        if ( ucode == 0x00000000 ) break;
        
        if ( ucode <= 0x0000001f ) {
            if ( ucode == 0x0000000a ) {
                //y += lineH;
                //x = x0;
            }
            continue;
        }

        glyph_index = FT_Get_Char_Index( face, ucode );

        if (use_kerning && previous && glyph_index) {
            FT_Vector delta;
            FT_Get_Kerning( face, previous, glyph_index, FT_KERNING_DEFAULT, &delta);
            pen.x += delta.x >> 6;
        }

        previous = glyph_index;
                
        error = FT_Load_Glyph(face, glyph_index, FT_LOAD_RENDER|FT_LOAD_NO_HINTING   );
        if (error) continue;                 /* ignore errors */
    
        draw_bitmap(slot, pen, bitmap);
    
        pen.x += slot->advance.x >> 6;
        pen.y += slot->advance.y >> 6;

        if (lines > 1 && ucode == ' ' && pen.x > width-(size*5)) {
            pen.x = 0;
            pen.y += size+4;
        }

    }  

    return bitmap;
}
