#include <stdio.h>
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "NRD_HAL.h"
#include "render.h"
#include <pthread.h>

static pthread_cond_t  cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

static AVFrame *frame0;

int digest_frame(unsigned char*** pixels, int** stride) {
    if (frame0) {

        if (pixels != NULL && stride != NULL) {

            AVFrame *frame = frame0;
            *pixels = frame->data;
            *stride = frame->linesize;

        } else {
            frame0 = NULL;
            pthread_cond_signal(&cond);
        }

        return 1;
    }
    return 0;
}

void print_error(const char *filename, int err) {
    char errbuf[128];
    const char *errbuf_ptr = errbuf;
    if (av_strerror(err, errbuf, sizeof(errbuf)) < 0)
        errbuf_ptr = strerror(AVUNERROR(err));
    av_log(NULL, AV_LOG_ERROR, "%s: %s\n", filename, errbuf_ptr);
}

static void init_video_decode(void *url) {

    AVFormatContext* ic = avformat_alloc_context();
    AVCodecContext *avctx;
    AVCodec *codec;
    AVDictionary *opts;

    AVFrame* frame = av_frame_alloc();
    AVPacket avpkt;
    av_init_packet(&avpkt);

    int ret = avformat_open_input (&ic, (char*)url, NULL, NULL);

    if (ret <0) {
        print_error((char*)url, ret);
        return;
    }

    int best_video_stream = av_find_best_stream(ic, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    int best_audio_stream = av_find_best_stream(ic, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);

    av_dump_format(ic, 0, (char*)url, 0);

    avctx = ic->streams[best_video_stream]->codec;
    codec = avcodec_find_decoder(avctx->codec_id);
    ret = avcodec_open2(avctx, codec, NULL);
    
    av_read_play(ic);
    int c = 0;
    for(;;) {

        int ret = av_read_frame(ic, &avpkt);
        if (ret < 0) {
            if (ret == AVERROR_EOF || (ic->pb && ic->pb->eof_reached))
                break;
            if (ic->pb && ic->pb->error)
                break;
            continue;
        }

        if(avpkt.stream_index == best_video_stream) {

            if (frame0) {
                pthread_mutex_lock(&lock);
                pthread_cond_wait(&cond, &lock);
                pthread_mutex_unlock(&lock);
            }

            ret = 0;
            avcodec_decode_video2(avctx, frame, &ret, &avpkt);
            av_free_packet(&avpkt);
            if (ret) {
                    frame0 = frame;
            }
        }

    }

}

void play_video(NRDHAL *api, void *url, void (*complete)()) {
    api->NRD_thread(init_video_decode, url);

}

void init_streamer() {
    avcodec_register_all();
    av_register_all();
    avformat_network_init();
}

