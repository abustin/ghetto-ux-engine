#include "data_loader.h"
#include <pthread.h>
#include "curl/curl.h"
#include "NRD_HAL.h"
 
//static pthread_t threads[CURL_THREADS];
#define CURL_THREADS 3

static int nextpoweroftwo(int x) {
    double logbase2 = math.log(x) / math.log(2);
    return math.round(math.pow(2,math.ceil(logbase2)));
}

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct DataLoadingNode *loader = (struct DataLoadingNode *)userp;
    struct MemoryStruct *buff = loader->buffer;
    //printf("WriteMemoryCallback %s\n", loader->url);

    if (loader->state != JOB_STATE_LOADING) {
        //printf("WriteMemoryCallback returned 0 %i\n",loader->state);
        return 0;
    }

    
    if (buff->memsize < buff->size + realsize + 1) {

        int p = nextpoweroftwo(buff->size + realsize + 1);
        buff->memory = (char *) mem.realloc(buff->memory, p);
        //printf("realloc %zu %i\n", mem->memsize, p);
        if(buff->memory == NULL) {
            /* out of memory! */ 
            mem.printf("not enough memory (realloc returned NULL)\n");
            return 0;
        }
        buff->memsize = p;
    }

//    printf("%.10s\n", contents);
    mem.memcpy(buff->memory+buff->size, contents, realsize);
    buff->size += realsize;
    buff->memory[buff->size] = 0;

    
    return realsize;    
    
}

int curl_prog(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow) {
    struct DataLoadingNode *loader = (struct DataLoadingNode *)clientp;
    if (loader->state != JOB_STATE_LOADING) {
        mem.printf("curl_prog abort 0 %s %i\n", loader->url, loader->state);
        return 1;
    } 
    return 0;
}

static void curl_json_native(CURL *curl_handle, struct DataLoadingNode *loader)  {
    int cid = loader->cycle;
    CURLcode res;
     //printf("%s\n", loader->url);
    loader->buffer->memory[0] = 0;
    loader->buffer->size = 0;

    curl_easy_reset(curl_handle);
    curl_easy_setopt(curl_handle, CURL_MAX_WRITE_SIZE, 40960);
    curl_easy_setopt(curl_handle, CURLOPT_NOSIGNAL, 1);
    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYHOST, 0);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "agent-bustin/1.0");
    curl_easy_setopt(curl_handle, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    curl_easy_setopt(curl_handle, CURLOPT_PROGRESSFUNCTION, curl_prog);
    #ifdef CURLOPT_TCP_KEEPALIVE
    curl_easy_setopt(curl_handle, CURLOPT_TCP_KEEPALIVE, 1);
    #endif
    curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 0);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)loader);
    curl_easy_setopt(curl_handle, CURLOPT_PROGRESSDATA, (void *)loader);
    curl_easy_setopt(curl_handle, CURLOPT_ACCEPT_ENCODING, "gzip");
    //curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
    curl_easy_setopt(curl_handle, CURLOPT_URL, loader->url);
    //mem.printf("start%s\n", loader->url);
    /* init the curl session */ 
    res = curl_easy_perform(curl_handle);
    //printf("end%s\n", loader->url);

    if(res == CURLE_OK) {
        http_get_complete(loader, cid);
    } else if (cid == loader->cycle) {
        if (loader->retry < 3) {
            mem.printf("retry!\n");
            loader->retry++;
            curl_json_native(curl_handle, loader);
        } else {
            loader->state = JOB_STATE_SUCCESS;
        }
    }

 
}

struct ThreadData {
    int id;
    int active;
    NRDThread thread;
};

pthread_cond_t  cond  = PTHREAD_COND_INITIALIZER;
pthread_mutex_t  lock = PTHREAD_MUTEX_INITIALIZER;
#include "pthread.h"

void wake_loader() {
    pthread_cond_signal(&cond);
}

static void loader_thread_loop(void *ptr) {

    struct ThreadData *td = ptr;

    mem.printf("Starting thread %i\n",td->id);

    struct DataLoadingNode* loader = NULL;
    
    struct MemoryStruct buffer;
    buffer.memory = (char*) mem.malloc(INIT_MEM_SIZE);
    buffer.memsize = INIT_MEM_SIZE;

    CURL *curl_handle = curl_easy_init();
    mem.printf("Starting thread!!! %i\n",td->id);

    while(td->active) {

        loader = get_next_aync_json_job();
        if (loader == NULL) {
            loader = get_next_aync_job();
        }

        if (loader != NULL) {
            loader->buffer = &buffer;
            loader->retry = 0;
            curl_json_native(curl_handle, loader);
        } else {
            pthread_mutex_lock(&lock);
            pthread_cond_wait(&cond, &lock);
            pthread_mutex_unlock(&lock);
        }
        //sched_yield();
    }


    mem.free(buffer.memory);
    curl_easy_cleanup(curl_handle);
    mem.printf("thread end\n");
}
 
static struct ThreadData *threads;

void data_loader_boot() {


    struct ThreadData *td = mem.calloc(CURL_THREADS, sizeof(struct ThreadData));
    threads = td;

    int i = CURL_THREADS;
    while (i--) {

        td->id = i;
        td->active = 1;
        td->thread = api->NRD_thread(loader_thread_loop, td);
        td++;
    }
 
}

void data_loader_shutdown() {

    mem.printf("data_loader_shutdown\n");
    struct ThreadData *td = threads;
    
    int i = CURL_THREADS;
    while (i--) {

        td->active = 0;
        wake_loader();
        api->NRD_thread_join(td->thread);
        mem.free(td->thread.thread);
        td++;
    }
 
    mem.free(threads);  
    mem.printf("data_loader_shutdown closed all threads\n");

    curl_global_cleanup();
}

