#ifndef _render_h_
#define _render_h_

#include "render_node.h"
//#include "texture_loader.h"
#include "NRD_HAL.h"


void drawColoredImage(struct RenderData *rd, GLuint tex, float x, float y, float z, float r, float g, float b, float a);
void drawImage(struct RenderData *rd, GLuint tex, float x, float y, float z);
void drawHighlight(struct RenderData *rd, float x, float y, float w, float h);
void drawSizedImage(struct RenderData *rd, unsigned int tex, float x, float y, float z, float w, float h);
void drawColorImage(struct RenderData *rd, unsigned int  tex, float x, float y, float z, float w, float h, float red,float green,float blue,float alpha);
void drawColorBox(struct RenderData *rd, float red,float green,float blue,float alpha,float x, float y, float z,float w, float h);
void drawZoltarImage(struct RenderData *rd, float x, float y, float w, float h, float interest, float slant, float effect, float bright);
void drawGalleryTile(struct RenderData *rd, unsigned int tex, float x, float y, float z, float w, float h, float v);
void drawVideoPlane(struct RenderData *rd, float x, float y, float w, float h, float wipe);
void drawVin(struct RenderData *rd, float x, float y, float w, float h, float wipe, float videoMode, int blend);

void loop(void (*innerLoop)(struct RenderData*));
void initGL();
void cleanGL();

#include "render_boot.h"
#include "render_program.h"





#endif 
