#include "render_boot.h"

#include "GLES.h"
#include <stdio.h>

#ifdef _GLUT
#ifdef __MACH__
#include <glut.h>
#else
#include <GL/glut.h>
#endif
extern void mainLoop();
// Called every time a window is resized to resize the projection matrix

void display()
{
  mainLoop();
}

void idle()
{
    mainLoop();
}

// Called every time a window is resized to resize the projection matrix
void reshape(int w, int h)
{

}

static int last_key = 0;

int get_last_key() {
  int key = last_key;
  last_key = 0;
  return key;
}

void keyboard_alpha(unsigned char key, int x, int y) {
  //printf("--- %i\n", key);
  last_key = key;
}

void keyboard(unsigned char key, int x, int y) {
  last_key = key;
  //printf("+++ %i\n", key);
}

void setupWindow(int w, int h) {


    printf("doing glutInitDisplayMode\n");
    // Sets up a double buffer with RGBA components and a depth component
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_MULTISAMPLE);
    printf("done glutInitDisplayMode\n");

    // Sets the window size to 512*512 square pixels
    glutInitWindowSize(w, h);
    printf("done glutInitWindowSize %i %i\n",w,h);
    // Sets the window position to the upper left
    glutInitWindowPosition(0, 0);
    printf("done glutInitWindowPosition\n");
    // Creates a window using internal glut functionality
    glutCreateWindow("NFLX");
    printf("done glutCreateWindow\n");

    // passes reshape and display functions to the OpenGL machine for callback
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutIdleFunc(idle);

    glutKeyboardFunc(keyboard_alpha);
    glutSpecialFunc(keyboard);

    //glClearColor(0.0, 0.0, 0.0, 0.0);
    //init();
    //printf("%s\n%s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION));

    // Starts the program.
    //glEnable(GL_MULTISAMPLE_ARB);
    //glEnable(GL_MULTISAMPLE);
    //return 0;

    printf("Done window setup\n");
}




void finishFrame() {
    //printf("finit\n");
    glutSwapBuffers();
    //SDL_GL_SwapWindow(window);
        //usleep(99);
}

#endif


#ifdef __ROKU__

/*

    ROKU_INPUT_BUTTON_INVALID           =  0x00000000,
    ROKU_INPUT_BUTTON_UP                =  0x00000001,
    ROKU_INPUT_BUTTON_DOWN              =  0x00000002,
    ROKU_INPUT_BUTTON_LEFT              =  0x00000004,
    ROKU_INPUT_BUTTON_RIGHT             =  0x00000008,
    ROKU_INPUT_BUTTON_SELECT            =  0x00000010,
    ROKU_INPUT_BUTTON_PLAY              =  0x00000020,
    ROKU_INPUT_BUTTON_RWD               =  0x00000040,
    ROKU_INPUT_BUTTON_FWD               =  0x00000080,
    ROKU_INPUT_BUTTON_BACK              =  0x00000100,
    ROKU_INPUT_BUTTON_INFO              =  0x00000200,
    ROKU_INPUT_BUTTON_INSTANT_REPLAY    =  0x00000400,
    ROKU_INPUT_BUTTON_A                 =  0x00000800,
    ROKU_INPUT_BUTTON_B                 =  0x00001000,
    ROKU_INPUT_BUTTON_PLAY_ONLY         =  0x00002000,
    ROKU_INPUT_BUTTON_STOP              =  0x00004000,
    ROKU_INPUT_BUTTON_ENTER             =  0x00008000,
    ROKU_INPUT_BUTTON_BACKSPACE         =  0x00010000,
    ROKU_INPUT_BUTTON_PARTNER           =  0x00020000, 


*/

#include <roku/input.h>
static RokuInputContext *roku_input_ctx;

static int key_repeat = 0;

int get_last_key() {

  if (key_repeat-- > 0) return;

  int button = 0;

    struct RokuControllerState* controller_state;
    RokuInputContext_getControllerState(roku_input_ctx, 0, &controller_state);
    unsigned int  rokuButton = RokuControllerState_getButtons(controller_state);
    RokuControllerState_release(controller_state);

    if((rokuButton & ROKU_INPUT_BUTTON_RIGHT) == ROKU_INPUT_BUTTON_RIGHT)
        button = 102;
    else if((rokuButton & ROKU_INPUT_BUTTON_LEFT) == ROKU_INPUT_BUTTON_LEFT)
        button = 100;
    else if((rokuButton & ROKU_INPUT_BUTTON_DOWN) == ROKU_INPUT_BUTTON_DOWN)
        button = 103;
    else if((rokuButton & ROKU_INPUT_BUTTON_UP) == ROKU_INPUT_BUTTON_UP)
        button = 101;
    else if((rokuButton & ROKU_INPUT_BUTTON_SELECT) == ROKU_INPUT_BUTTON_SELECT)
        button = 13;
    else if((rokuButton & ROKU_INPUT_BUTTON_FWD) == ROKU_INPUT_BUTTON_FWD)
        button = 400;
    else if((rokuButton & ROKU_INPUT_BUTTON_RWD) == ROKU_INPUT_BUTTON_RWD)
        button = 401;

      if (button) {
        key_repeat = 3;
      }

    return button;

}

int get_last_key2() {

    RokuInputEvent *event;
    int button = 0;

    if(RokuInputContext_getEvent(roku_input_ctx, &event) == 1) {
        int type = RokuInputEvent_getType(event);
        
        if(type == ROKU_INPUT_EVENT_TYPE_BUTTON_PRESSED) {
            int rokuButton = RokuInputEvent_getButton(event);

            if(rokuButton == ROKU_INPUT_BUTTON_RIGHT)
                button = 102;
            else if(rokuButton == ROKU_INPUT_BUTTON_LEFT)
                button = 100;
            else if(rokuButton == ROKU_INPUT_BUTTON_DOWN)
                button = 103;
            else if(rokuButton == ROKU_INPUT_BUTTON_UP)
                button = 101;
            else if(rokuButton == ROKU_INPUT_BUTTON_SELECT)
                button = 13;
        }
       
        RokuInputEvent_release(event);
    }

    return button;
}

typedef struct
{
   EGLDisplay display;
   EGLSurface surface;
   EGLContext context;  
} EGL_STATE_T;

#define check() assert(glGetError() == 0)

static EGL_STATE_T state;

void setupWindow(int w, int h) {

  RokuInputContext_createWithFilter(&roku_input_ctx, ROKU_CONTEXT_FILTER_NONE);

    uint32_t screen_width = 0;
    uint32_t screen_height = 0;

   memset( &state, 0, sizeof(EGL_STATE_T));

   int32_t success = 0;
   EGLBoolean result;
   EGLint num_config;

   static const EGLint attribute_list[] =
   {
      EGL_RED_SIZE, 1,
      EGL_GREEN_SIZE, 1,
      EGL_BLUE_SIZE, 1,
      EGL_ALPHA_SIZE, 1,
      EGL_DEPTH_SIZE, 0,
      EGL_STENCIL_SIZE, 0,
      EGL_BUFFER_SIZE,EGL_DONT_CARE,
      EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
      EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
      EGL_SAMPLE_BUFFERS,1,
      EGL_SAMPLES,4,
      EGL_NONE
   };
   
   static const EGLint context_attributes[] = 
   {
      EGL_CONTEXT_CLIENT_VERSION, 2,
      EGL_NONE
   };
   EGLConfig config;


   const EGLint egl_surface_attribs[] = {
               EGL_RENDER_BUFFER, EGL_BACK_BUFFER,
                   EGL_NONE,
   };

   state.display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
   
   result = eglInitialize(state.display, NULL, NULL);
printf("resulti: %i\n",result);   
   result = eglChooseConfig(state.display, attribute_list, &config, 1, &num_config);
  printf("result: %i\n",result);  
   result = eglBindAPI(EGL_OPENGL_ES_API);
// create an EGL rendering context
   state.context = eglCreateContext(state.display, config, EGL_NO_CONTEXT, context_attributes);
   state.surface = eglCreateWindowSurface( state.display, config,  (NativeWindowType) 1, egl_surface_attribs );
   
   // connect the context to the surface
   result = eglMakeCurrent(state.display, state.surface, state.surface, state.context);
   
   eglSwapInterval(state.display, 1);
printf("result: %i\n",result); 
}



void finishFrame() {
    glFinish();
    eglSwapBuffers(state.display, state.surface);
}


#endif

#ifdef __NEXUS__

#include "esutil.h"
#include "default_nexus.h"
#include "init.h"
#include "nxclient.h"
#include "nexus_types.h"
//#include "default_nexus.h"
#include "nexus_platform.h"
#include "nexus_display.h"
#include "nexus_ir_input.h"
#include "nexus_input_client.h"



typedef struct
{
   bool     useMultisample;
   bool     usePreservingSwap;
   bool     stretchToFit;
   int      x;
   int      y;
   int      vpW;
   int      vpH;
   int      bpp;
   int      frames;
   unsigned clientId;
   unsigned zOrder;
} AppConfig;

static NEXUS_DISPLAYHANDLE  nexus_display = 0;
static AppConfig            config;
static EGLNativeDisplayType native_display = 0;
static void                 *native_window = 0;
static float                panelAspect = 1.0f;

NXPL_PlatformHandle nxpl_handle = 0;
NxClient_AllocSettings      fAllocSettings_;
NxClient_AllocResults       fAllocResults_;
NEXUS_InputClientHandle     fIClient_;
NEXUS_InputClientSettings   fISettings_;
BKNI_EventHandle            fEvent_;
unsigned int                fIClientId_;
bool                        fHasData_;




// IR handler callback
static void fIrCallback(void *pParam, int iParam)
{
    BSTD_UNUSED(iParam);
    BSTD_UNUSED(pParam);

    fHasData_ = true;

    return;
}

// Return Key info
static int repeat_count = 0;
int get_last_key()
{
     if (!fHasData_) return 0;

     NEXUS_InputRouterCode ncode;
     NEXUS_Error rc;
     int num = 0;

     rc = NEXUS_InputClient_GetCodes(fIClient_, &ncode, 1, &num);

     if (rc != NEXUS_SUCCESS)
     {
        return 0;
     }
     if (ncode.data.irInput.repeat == 0) {
        repeat_count = 0;
     } else {
        repeat_count++;
     }

     if ( ncode.deviceType == NEXUS_InputRouterDevice_eIrInput && repeat_count!=1)
     {
        switch((unsigned int) ncode.data.irInput.code) {
                case 3058302720: return 102;
                case 4094426880: return 100;
                case 2974744320: return 101;
                case 4077715200: return 103;
        }
         printf("%u %u %u\n", ncode.data.irInput.code, ncode.data.irInput.repeat,ncode.data.irInput.mode);
         fHasData_ = false;
     }

     return num;
}







static void BppToChannels(int bpp, int *r, int *g, int *b, int *a)
{
   switch (bpp)
   {
   default:
   case 16:             /* 16-bit RGB (565)  */
      *r = 5;
      *g = 6;
      *b = 5;
      *a = 0;
      break;

   case 32:             /* 32-bit RGBA       */
      *r = 8;
      *g = 8;
      *b = 8;
      *a = 8;
      break;

   case 24:             /* 24-bit RGB        */
      *r = 8;
      *g = 8;
      *b = 8;
      *a = 0;
      break;
   }
}


bool InitEGL(NativeWindowType egl_win, const AppConfig *config)
{
   EGLDisplay egl_display      = 0;
   EGLSurface egl_surface      = 0;
   EGLContext egl_context      = 0;
   EGLConfig *egl_config;
   EGLint     major_version;
   EGLint     minor_version;
   int        config_select    = 0;
   int        configs;

   /*
      Specifies the required configuration attributes.
      An EGL "configuration" describes the pixel format and type of
      surfaces that can be used for drawing.
      For now we just want to use a 16 bit RGB surface that is a
      Window surface, i.e. it will be visible on screen. The list
      has to contain key/value pairs, terminated with EGL_NONE.
   */
   int   want_red   = 0;
   int   want_green = 0;
   int   want_blue  = 0;
   int   want_alpha = 0;

   BppToChannels(config->bpp, &want_red, &want_green, &want_blue, &want_alpha);

   /*
      Step 1 - Get the EGL display.
   */
   egl_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
   if (egl_display == EGL_NO_DISPLAY)
   {
      printf("eglGetDisplay() failed, did you register any exclusive displays\n");
      return false;
   }

   /*
      Step 2 - Initialize EGL.
      EGL has to be initialized with the display obtained in the
      previous step. We cannot use other EGL functions except
      eglGetDisplay and eglGetError before eglInitialize has been
      called.
   */
   if (!eglInitialize(egl_display, &major_version, &minor_version))
   {
      printf("eglInitialize() failed\n");
      return false;
   }

   /*
      Step 3 - Get the number of configurations to correctly size the array
      used in step 4
   */
   if (!eglGetConfigs(egl_display, NULL, 0, &configs))
   {
      printf("eglGetConfigs() failed\n");
      return false;
   }

   egl_config = (EGLConfig *)alloca(configs * sizeof(EGLConfig));

   /*
      Step 4 - Find a config that matches all requirements.
      eglChooseConfig provides a list of all available configurations
      that meet or exceed the requirements given as the second
      argument.
   */

   {
      const int   NUM_ATTRIBS = 21;
      EGLint      *attr = (EGLint *)malloc(NUM_ATTRIBS * sizeof(EGLint));
      int         i = 0;

      attr[i++] = EGL_RED_SIZE;        attr[i++] = want_red;
      attr[i++] = EGL_GREEN_SIZE;      attr[i++] = want_green;
      attr[i++] = EGL_BLUE_SIZE;       attr[i++] = want_blue;
      attr[i++] = EGL_ALPHA_SIZE;      attr[i++] = want_alpha;
      attr[i++] = EGL_DEPTH_SIZE;      attr[i++] = 24;
      attr[i++] = EGL_STENCIL_SIZE;    attr[i++] = 0;
      attr[i++] = EGL_SURFACE_TYPE;    attr[i++] = EGL_WINDOW_BIT;
      attr[i++] = EGL_RENDERABLE_TYPE; attr[i++] = EGL_OPENGL_ES2_BIT;

      if (config->useMultisample)
      {
         attr[i++] = EGL_SAMPLE_BUFFERS; attr[i++] = 1;
         attr[i++] = EGL_SAMPLES;        attr[i++] = 4;
      }

      attr[i++] = EGL_NONE;

      //assert(i <= NUM_ATTRIBS);

      if (!eglChooseConfig(egl_display, attr, egl_config, configs, &configs) || (configs == 0))
      {
         printf("eglChooseConfig() failed");
         return false;
      }

      free(attr);
   }

   for (config_select = 0; config_select < configs; config_select++)
   {
      /*
         Configs with deeper color buffers get returned first by eglChooseConfig.
         Applications may find this counterintuitive, and need to perform additional processing on the list of
         configs to find one best matching their requirements. For example, specifying RGBA depths of 565
         could return a list whose first config has a depth of 888.
      */

      /* Check that config is an exact match */
      EGLint red_size, green_size, blue_size, alpha_size, depth_size;

      eglGetConfigAttrib(egl_display, egl_config[config_select], EGL_RED_SIZE,   &red_size);
      eglGetConfigAttrib(egl_display, egl_config[config_select], EGL_GREEN_SIZE, &green_size);
      eglGetConfigAttrib(egl_display, egl_config[config_select], EGL_BLUE_SIZE,  &blue_size);
      eglGetConfigAttrib(egl_display, egl_config[config_select], EGL_ALPHA_SIZE, &alpha_size);
      eglGetConfigAttrib(egl_display, egl_config[config_select], EGL_DEPTH_SIZE, &depth_size);

      if ((red_size == want_red) && (green_size == want_green) && (blue_size == want_blue) && (alpha_size == want_alpha))
      {
         printf("Selected config: R=%d G=%d B=%d A=%d Depth=%d\n", red_size, green_size, blue_size, alpha_size, depth_size);
         break;
      }
   }

   if (config_select == configs)
   {
      printf("No suitable configs found\n");
      return false;
   }

   /*
      Step 5 - Create a surface to draw to.
      Use the config picked in the previous step and the native window
      handle to create a window surface. 
   */
   egl_surface = eglCreateWindowSurface(egl_display, egl_config[config_select], egl_win, NULL);
   if (egl_surface == EGL_NO_SURFACE)
   {
      eglGetError(); /* Clear error */
      egl_surface = eglCreateWindowSurface(egl_display, egl_config[config_select], NULL, NULL);
   }

   if (egl_surface == EGL_NO_SURFACE)
   {
      printf("eglCreateWindowSurface() failed\n");
      return false;
   }

   /* Only use preserved swap if you need the contents of the frame buffer to be preserved from
    * one frame to the next
    */
   if (config->usePreservingSwap)
   {
      printf("Using preserved swap.  Application will run slowly.\n");
      eglSurfaceAttrib(egl_display, egl_surface, EGL_SWAP_BEHAVIOR, EGL_BUFFER_PRESERVED);
   }

   /*
      Step 6 - Create a context.
      EGL has to create a context for OpenGL ES. Our OpenGL ES resources
      like textures will only be valid inside this context (or shared contexts)
   */
   {
      EGLint     ctx_attrib_list[3] =
      {
           EGL_CONTEXT_CLIENT_VERSION, 2, /* For ES2 */
           EGL_NONE
      };

      egl_context = eglCreateContext(egl_display, egl_config[config_select], EGL_NO_CONTEXT, ctx_attrib_list);
      if (egl_context == EGL_NO_CONTEXT)
      {
         printf("eglCreateContext() failed");
         return false;
      }
   }

   /*
      Step 7 - Bind the context to the current thread and use our
      window surface for drawing and reading.
      We need to specify a surface that will be the target of all
      subsequent drawing operations, and one that will be the source
      of read operations. They can be the same surface.
   */
   eglMakeCurrent(egl_display, egl_surface, egl_surface, egl_context);

   eglSwapInterval(egl_display, 1);

   return true;
}

bool InitDisplay(float *aspect, const AppConfig *config)
{
   NXPL_NativeWindowInfo   win_info;

   eInitResult res = InitPlatformAndDefaultDisplay(&nexus_display, aspect, config->vpW, config->vpH);
   if (res != eInitSuccess)
      return false;

   /* Register with the platform layer */
   NXPL_RegisterNexusDisplayPlatform(&nxpl_handle, nexus_display);

   win_info.x = config->x;
   win_info.y = config->y;
   win_info.width = config->vpW;
   win_info.height = config->vpH;
   win_info.stretch = config->stretchToFit;
   win_info.clientID = config->clientId;
   win_info.zOrder = config->zOrder;
   native_window = NXPL_CreateNativeWindow(&win_info);

   /* Initialise EGL now we have a 'window' */
   if (!InitEGL(native_window, config))
      return false;
    NEXUS_Error rc = NEXUS_NOT_SUPPORTED;
/* Enable IR input */
    rc = BKNI_CreateEvent(&fEvent_);
    if (rc)
    {
        abort();
    }

    NxClient_GetDefaultAllocSettings(&fAllocSettings_);
    fAllocSettings_.inputClient = 1;
    rc = NxClient_Alloc(&fAllocSettings_, &fAllocResults_);
    if (rc)
    {
        abort();
    }

    fIClient_ = NEXUS_InputClient_Acquire(fIClientId_= fAllocResults_.inputClient[0].id);
    if (!fIClient_)
    {
        abort();
    }

    NEXUS_InputClient_GetSettings(fIClient_, &fISettings_);
    fISettings_.filterMask = 0;
    fISettings_.filterMask |= 1<<NEXUS_InputRouterDevice_eIrInput;
    fISettings_.codeAvailable.callback = fIrCallback;
    fISettings_.codeAvailable.context = fEvent_;
    rc = NEXUS_InputClient_SetSettings(fIClient_, &fISettings_);
    if (rc)
    {
        abort();
    }

   return true;
}

void setupWindow(int w, int h) {

   config.useMultisample    = true;
   config.usePreservingSwap = false;
   config.stretchToFit      = false;
   config.x                 = 0;
   config.y                 = 0;
   config.vpW               = w;
   config.vpH               = h;
   config.bpp               = 32;
   config.frames            = 0;
   config.clientId          = 0;
   config.zOrder            = 0;

   InitDisplay(&panelAspect, &config);

}

void finishFrame() {
    eglSwapBuffers(eglGetCurrentDisplay(), eglGetCurrentSurface(EGL_READ));
}



#endif




#ifdef __PI__

typedef struct
{
   EGLDisplay display;
   EGLSurface surface;
   EGLContext context;  
} EGL_STATE_T;

#define check() assert(glGetError() == 0)

static EGL_STATE_T state;

void setupWindow(int w, int h) {

    uint32_t screen_width = 0;
    uint32_t screen_height = 0;

   memset( &state, 0, sizeof(EGL_STATE_T));
   printf("doing bcm_host_init\n");
   bcm_host_init();

   int32_t success = 0;
   EGLBoolean result;
   EGLint num_config;

   //static EGL_DISPMANX_WINDOW_T nativewindow;

   DISPMANX_ELEMENT_HANDLE_T dispman_element;
   DISPMANX_DISPLAY_HANDLE_T dispman_display;
   DISPMANX_UPDATE_HANDLE_T dispman_update;
   VC_RECT_T dst_rect;
   VC_RECT_T src_rect;

   static const EGLint attribute_list[] =
   {
      EGL_RED_SIZE, 8,
      EGL_GREEN_SIZE, 8,
      EGL_BLUE_SIZE, 8,
      EGL_ALPHA_SIZE, 8,
      EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
      EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
      EGL_SAMPLE_BUFFERS,1,
      EGL_SAMPLES,4
      EGL_NONE
   };
   
   static const EGLint context_attributes[] = 
   {
      EGL_CONTEXT_CLIENT_VERSION, 2,
      EGL_NONE
   };
   EGLConfig config;

   printf("doing eglGetDisplay\n");
   // get an EGL display connection
   state.display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
   assert(state.display!=EGL_NO_DISPLAY);
   check();

   printf("doing eglInitialize\n");
   // initialize the EGL display connection
   result = eglInitialize(state.display, NULL, NULL);
   assert(EGL_FALSE != result);
   check();

   printf("doing eglChooseConfig\n");
   // get an appropriate EGL frame buffer configuration
   result = eglChooseConfig(state.display, attribute_list, &config, 1, &num_config);
   assert(EGL_FALSE != result);
   check();

   printf("doing eglBindAPI\n");
   // get an appropriate EGL frame buffer configuration
   result = eglBindAPI(EGL_OPENGL_ES_API);
   assert(EGL_FALSE != result);
   check();

   printf("doing eglCreateContext\n");
   // create an EGL rendering context
   state.context = eglCreateContext(state.display, config, EGL_NO_CONTEXT, context_attributes);
   assert(state.context!=EGL_NO_CONTEXT);
   check();

   printf("doing graphics_get_display_size\n");
   // create an EGL window surface
   success = graphics_get_display_size(0 /* LCD */, &screen_width, &screen_height);
   assert( success >= 0 );

   printf("w=%u h=%u\n", screen_width, screen_height);

   dst_rect.x = 0;
   dst_rect.y = 0;
   dst_rect.width = screen_width;
   dst_rect.height = screen_height;
      
   src_rect.x = 0;
   src_rect.y = 0;
   src_rect.width = screen_width << 16;
   src_rect.height = screen_height << 16;        
   printf("doing vc_dispmanx_display_open\n");
   dispman_display = vc_dispmanx_display_open( 0 /* LCD */);
   printf("doing vc_dispmanx_update_start\n");
   dispman_update = vc_dispmanx_update_start( 0 );
         
   printf("doing vc_dispmanx_element_add\n");
   dispman_element = vc_dispmanx_element_add ( dispman_update, dispman_display,
      0/*layer*/, &dst_rect, 0/*src*/,
      &src_rect, DISPMANX_PROTECTION_NONE, 0 /*alpha*/, 0/*clamp*/, 0/*transform*/);
      
   nativewindow.element = dispman_element;
   nativewindow.width = screen_width;
   nativewindow.height = screen_height;
   printf("doing vc_dispmanx_update_submit_sync\n");
   vc_dispmanx_update_submit_sync( dispman_update );
      
   check();
   printf("doing eglCreateWindowSurface\n");
   state.surface = eglCreateWindowSurface( state.display, config, &nativewindow, NULL );
   assert(state.surface != EGL_NO_SURFACE);
   check();

   // connect the context to the surface
   result = eglMakeCurrent(state.display, state.surface, state.surface, state.context);
   assert(EGL_FALSE != result);
   check();

   check();
}



void finishFrame() {
    eglSwapBuffers(state.display, state.surface);
}


#endif
