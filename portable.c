#include "NRD_HAL.h"
#include "render_node.h"
#include "render.h"
#include "texture_loader.h"
#include "async_job_queue.h"
#include <stdio.h>
#define bind_sym(OBJ, FUNC) OBJ.FUNC = nrd_api->NRD_global_sym( #FUNC );
#include "text_loader.h"

#include <stdlib.h>
NRDHAL *api;
struct GLWrapper gl;
struct MathWrapper math;
struct MemWrapper mem;

void network_streamer(char*, http_write_callback, http_finish_callback, http_progress_callback, void *);

void render_loop(RenderData *ctx);
void clean_app_data();

static void lib_loop() {
    check_waiting(should_cancel_texture_load);
    loop(render_loop);
}

void NRD_end_module(NRDHAL* nrd_api) {
    mem.printf("%s\n", "NRD_end_module");
    cleanGL();
    async_job_queue_shutdown();
    clean_app_data();
}

void NRD_start_module(NRDHAL* nrd_api) {

    initText();
    

    printf("hello world\n");

    bind_sym(math, sin);
    bind_sym(math, cos);
    bind_sym(math, div);

    bind_sym(math, sin);
    bind_sym(math, cos);
    bind_sym(math, div);

    bind_sym(math, log);
    bind_sym(math, round);
    bind_sym(math, ceil);
    bind_sym(math, pow);
    bind_sym(math, sqrt);
    bind_sym(math, atan2);
    bind_sym(math, asin);
    
    
    bind_sym(mem, strncmp);
    bind_sym(mem, memcpy);
    bind_sym(mem, memset);
    bind_sym(mem, printf);
    bind_sym(mem, malloc);
    bind_sym(mem, calloc);
    bind_sym(mem, free);
    bind_sym(mem, realloc);
    bind_sym(mem, strcat);
    bind_sym(mem, strdup);
    bind_sym(mem, strcpy);
    bind_sym(mem, strstr);
    bind_sym(mem, tolower);
    
    bind_sym(gl, glGetShaderInfoLog);
    bind_sym(gl, glGetShaderiv);
    bind_sym(gl, printOpenGLError);
    bind_sym(gl, glPixelStorei);
    bind_sym(gl, glDeleteTextures);
    bind_sym(gl, glActiveTexture);
    bind_sym(gl, glBindTexture);
    bind_sym(gl, glBlendFunc);
    bind_sym(gl, glClear);
    bind_sym(gl, glClearColor);
    bind_sym(gl, glDrawArrays);
    bind_sym(gl, glGenTextures);
    bind_sym(gl, glGetError);
    bind_sym(gl, glTexImage2D);
    bind_sym(gl, glTexParameteri);
    bind_sym(gl, glTexSubImage2D);
    bind_sym(gl, glUniform1f);
    bind_sym(gl, glUniform1i);
    bind_sym(gl, glUniform4f);
    bind_sym(gl, glUniformMatrix4fv);
    bind_sym(gl, glViewport);
    bind_sym(gl, glCreateShader);
    bind_sym(gl, glShaderSource);
    bind_sym(gl, glCompileShader);
    bind_sym(gl, glCreateProgram);
    bind_sym(gl, glAttachShader);
    bind_sym(gl, glUseProgram);
    bind_sym(gl, glGetUniformLocation);
    bind_sym(gl, glLinkProgram);
    bind_sym(gl, glEnable);
    bind_sym(gl, glDisable);
    bind_sym(gl, glGetAttribLocation);
    bind_sym(gl, glEnableVertexAttribArray);
    bind_sym(gl, glBindBuffer);
    bind_sym(gl, glGenBuffers);
    bind_sym(gl, glBufferData);
    bind_sym(gl, glVertexAttribPointer);
    bind_sym(gl, glFrontFace);
    bind_sym(gl, glCullFace);
    
    nrd_api->NRD_http_streamer = network_streamer;
    nrd_api->loop = lib_loop;
    api = nrd_api;
    initGL();
    async_job_queue_boot();
    mem.printf("%s\n", "Hello v8");

    
    


}

