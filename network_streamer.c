#include "curl/curl.h"
#include "NRD_HAL.h"
#include <string.h>
#include <stdlib.h>

typedef struct {
    char* url;
    http_write_callback write_cb;
    http_finish_callback finish_cb;
    http_progress_callback prog_cb;
    void *userdata;
} NetworkThread;


static void network_streamer_thread(void* tdata) {

    NetworkThread data  = *((NetworkThread*)tdata);
    free(tdata);

    printf("network_streamer_thread %s\n", data.url);

    CURL *curl_handle = curl_easy_init();

    
    curl_easy_setopt(curl_handle, CURLOPT_NOSIGNAL, 1);
    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYHOST, 0);
    if (data.write_cb) curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, data.write_cb);
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "agent-bustin/1.0");
    curl_easy_setopt(curl_handle, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    if (data.prog_cb) curl_easy_setopt(curl_handle, CURLOPT_PROGRESSFUNCTION, data.prog_cb);
    curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 0);
    curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, data.userdata);
    curl_easy_setopt(curl_handle, CURLOPT_URL, data.url);

    int ret = (curl_easy_perform(curl_handle) == CURLE_OK) ? 0 : -1;

    curl_easy_cleanup(curl_handle); 

    if (data.finish_cb) data.finish_cb(ret, data.userdata);

}

void network_streamer(char* url, http_write_callback write_cb, http_finish_callback finish_cb, http_progress_callback prog_cb, void *userdata) {


    printf("network_streamer %s\n", url);
    NetworkThread data = {url, write_cb, finish_cb, prog_cb, userdata};

    void* data_ref = malloc(sizeof(NetworkThread));
    memcpy(data_ref, (void*) &data, sizeof(NetworkThread));   

    api->NRD_thread(network_streamer_thread, data_ref);

}



