#ifndef _common_gles_h_
#define _common_gles_h_

#ifdef __MACH__
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
#elif __PI__ 
    #include <stdio.h>
    #include <fcntl.h>
    #include <stdlib.h>
    #include <string.h>
    #include <assert.h>
    #include <unistd.h>
    #include "bcm_host.h"
    #include "GLES2/gl2.h"
    #include "EGL/egl.h"
    #include "EGL/eglext.h"
#elif __ROKU__ 
    #include <stdio.h>
    #include <fcntl.h>
    #include <stdlib.h>
    #include <string.h>
    #include <assert.h>
    #include <unistd.h>
    #include "GLES2/gl2.h"
    #include "EGL/egl.h"
    #include "EGL/eglext.h"
#else
    #include <GLES2/gl2.h>
    #include <GLES2/gl2ext.h>
#endif

#endif
