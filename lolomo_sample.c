#include "NRD_HAL.h"
#include "render_node.h"
#include "render.h"
#include "async_job_queue.h"
#include "texture_loader.h"
#include <string.h>
#define LOLOMO_JSON_URL "http://54.69.9.35/whole_lolomo.json"


#define TILE_W (350.0*0.69)
#define TILE_H (197.0*0.69)
#define TITLE_W (612*0.69)
#define TITLE_H (260*0.69)
#define HERO_TIME (60*2);



typedef struct { float x,y,w,h,a; } Pos;

typedef struct {
    float startTime;
    float endTime;
    float touchTime;
    float initalVelocity;
    float currentVelocity;
    float from;
    float current;
    float to;
} AnimationProp;

typedef struct {
    char *title;
    char *desc;
    float rating;
    float title_alpha;
    float boxshot_alpha;
    int hero_count;
    int hero_idx;
    char* video_merch;
    char* video_stream;
    struct TextureUrlNode *title_img;
    struct TextureUrlNode *boxshot;
    struct TextureUrlNode *hero[3];
} ItemData;

typedef struct {
    int idx;
    char *title;
    ItemData *items;
    int count;
    AnimationProp position;
    float offset;
    float rate;
} RowData;

typedef struct {
    RowData *rows;
    int count;
    AnimationProp position;
} LolomoData;

RowData *selected_row;
//static float scrollDown;
//static float scrollDownTarget;
static LolomoData *lolomo_data;
static ItemData *selected_item;
static ItemData *old_selected_item;
static int lolomo_load_status = 0;
static int hero_cycle = 0;

static TextureUrlNode* tex_logo;
static TextureUrlNode* thanku;
//static TextureUrlNode tex_logo;

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define PADDING 0.97

#define TESTVIDEO0 "http://54.69.9.35/bojack.ts"
#define TESTVIDEO "http://54.69.9.35/UnbreakableKimmySchmidt.ts"

/*= DATA =============================================================*/

void clean_app_data() {
    mem.printf("clean_app_data\n");
    unsigned int row_len = lolomo_data->count;

    while (row_len--) mem.free(lolomo_data->rows[row_len].items);
    
    mem.free(lolomo_data->rows);
    mem.free(lolomo_data);
    mem.printf("clean_app_data done\n");
}

static char* getJSONStr(cJSON *json) {
    return (json && json->valuestring) ? mem.strdup(json->valuestring) : mem.calloc(1,1);
}

static char* getJSONObjStr(cJSON *json, char * key) {
    json = cJSON_GetObjectItem(json, key);
    return getJSONStr(json);
}

static void parseNetflixCategory(RowData *node, cJSON *json) {
    if (json == NULL) return; 

    cJSON *items_json = cJSON_GetObjectItem(json,"items");
    int len = cJSON_GetArraySize(items_json);
    int i;
    ItemData *items = mem.calloc(len, sizeof(ItemData));

    node->title = getJSONObjStr(json, "title");
    node->count = len;
    node->items = items;

    for (i=0; i<len; i++) {
        cJSON *item_json = cJSON_GetArrayItem(items_json, i);
        cJSON *heros = cJSON_GetObjectItem(item_json, "heros");

        items[i].hero_count = cJSON_GetArraySize(heros);

        if (heros) switch(items[i].hero_count) {
            case 3: items[i].hero[2] = getTexture(getJSONStr(cJSON_GetArrayItem(heros, 2)));
            case 2: items[i].hero[1] = getTexture(getJSONStr(cJSON_GetArrayItem(heros, 1)));
            case 1: items[i].hero[0] = getTexture(getJSONStr(cJSON_GetArrayItem(heros, 0)));
        }   

        cJSON *merch_video_json = cJSON_GetObjectItem(item_json,"merch_video");

        if (merch_video_json) {
            items[i].video_merch = getJSONStr(merch_video_json);
        }
        items[i].boxshot = getTexture(getJSONStr(cJSON_GetObjectItem(item_json,"boxshot")));
        items[i].desc = getJSONStr(cJSON_GetObjectItem(item_json,"synopsis"));
        items[i].title = getJSONStr(cJSON_GetObjectItem(item_json,"title"));        
        items[i].title_img = getTexture(getJSONStr(cJSON_GetObjectItem(item_json,"title_art")));
    }

}

static void parseNetflixData(LolomoData *node, cJSON *categories) {

    if (categories == NULL) return;
    
    int len = cJSON_GetArraySize(categories);
    RowData *cats = mem.calloc(len, sizeof(RowData));

    node->count = len;
    node->rows = cats;

    int i;
    for (i=0; i<len; i++) {
        (cats+i)->idx = i;
        parseNetflixCategory(cats+i, cJSON_GetArrayItem(categories,i));
    }
}

static void lolomo_json_callback(void *ctx, cJSON* json) {
    lolomo_data = mem.calloc(1, sizeof(LolomoData));
    parseNetflixData(lolomo_data, json);
    cJSON_Delete(json);
    lolomo_load_status  = 2;
}

static unsigned int modulo(unsigned int a, unsigned int b) {
    return (((a%b)+b)%b);
}

/*= ANIMATION HELPERS =============================================================*/

float clamp(float x, float min, float max) {
    if (x < min) x = min;
    else if (x > max) x = max;
    return x;
}

float smoothstep(float edge0, float edge1, float x) {
    x = clamp((x - edge0)/(edge1 - edge0), 0.0, 1.0); 
    return x*x*(3 - 2*x);
} 

static float blendedTween(float duration, float delta, float velocity, float t) {


    float d2 = duration*duration;
    
    float a = (3*velocity/d2 - 6*delta/(d2*duration)) / 3;
    float b = (6*delta/d2 - 4*velocity/duration) * 0.5;

    return a * (t*t*t) + b * (t*t) + velocity * t;
}

static void update(AnimationProp *prop, float currentTime) {

    currentTime = MAX(MIN(currentTime, prop->endTime), prop->startTime);

    float timeDelta = prop->endTime - prop->startTime;
    float updateTime = currentTime - prop->touchTime;

    if (!(timeDelta > 0 && updateTime >0)) {
        prop->currentVelocity =0;
        return;
    }

    
    float valueDelta = prop->to - prop->from;
    float timeProgress = currentTime - prop->startTime;
    
    float valueCurrent = prop->from;
    
    valueCurrent += blendedTween(timeDelta, valueDelta, prop->initalVelocity, timeProgress);
    
    prop->currentVelocity = (valueCurrent - prop->current) / updateTime;
    prop->current = valueCurrent;
    prop->touchTime = currentTime;
    
}

static void animateTo(AnimationProp *prop, float t, float delta, float duration) {
    prop->from = prop->current;
    prop->to += delta;
    prop->startTime = t;
    prop->endTime = t + duration;
    prop->initalVelocity = prop->currentVelocity;
    prop->touchTime = t;
}

/*= UI =============================================================*/

typedef enum {
    GALLERY, ZOLTAR, MDP, PLAYBACK
} UI_VIEW;

static UI_VIEW current_view = ZOLTAR;

typedef struct {
    int mode; // zoltar 0, montage 1, full 2
    int state; // closed 0, loading 1, playing 2
    char *url;
} VideoState;

static VideoState video_state;

static int frame_uses_video = 0;

static int playVideo(char *url) {
    
    
    frame_uses_video =1;
    if (url != video_state.url) {
        printf("playVideo %s\n", url);
        purgeLru(0,0);
        video_state.url = url;
        video_state.state = 1;
        
        switch (current_view) {
            case ZOLTAR:  
                api->NRD_set_video_window(-100,0,1280,720); 
                break;
            case GALLERY: 
                api->NRD_set_video_window(359,0,921,513);
                break;
            default:
                api->NRD_set_video_window(0,0,1280,720);
                break;
        }

        api->NRD_play_video(url);
    }
    return api->NRD_get_video_state();
}

static void stopVideo() {
    video_state.state = 0;
    video_state.url = NULL;
    api->NRD_close_video();
}

static int toInt(float d) {
    return (int) floor(d + 0.5);
}

static float wipe_in = 1;
static int show_prev_hero = 0;
static float wipe_time = 30;
static int idle_timeout = 0;
static float out_alpha = 0;

static float zoltar_navmode = 0;
static float zoltar_boxmode = 0;
static float lolomo_mdpmode = 0;
AnimationProp zoltar_scroll;
AnimationProp zoltar_mode;
AnimationProp lolomo_mode;
AnimationProp demo0;

static char* mdp_menu_items[] = {"Play Episode", "More Episodes", "Add to My List", "Rate this title", "Audio and Subtitles"};
static char* mdp_menu_icon[] = {
    "http://54.69.9.35/img/sdp_menu_icon_play.png", 
    "http://54.69.9.35/img/sdp_menu_icon_more_episodes.png", 
    "http://54.69.9.35/img/sdp_menu_icon_add.png", 
    "http://54.69.9.35/img/sdp_menu_icon_rate.png", 
    "http://54.69.9.35/img/sdp_menu_icon_audio_subs.png"
};
static TextureUrlNode** mdp_menu_icon_tex;

static float gallery_video_fade = 0;
static float mdp_video_fade = 0;


static void init_icons() {

    tex_logo = getTexture("http://54.69.9.35/netflixLogo.png");
    thanku = getTexture("http://54.69.9.35/thanku.jpg");

    int len = sizeof(mdp_menu_items)/sizeof(char*);
    int i;

    mdp_menu_icon_tex = calloc(len, sizeof( TextureUrlNode*));
    
    for (i=0;i<len;i++) {
        mdp_menu_icon_tex[i] = getTexture(mdp_menu_icon[i]);
    }

}

static void render_mdp(RenderData *ctx, Pos pos) {

    float mdp =  lolomo_mode.current;

    if (!mdp) return;



    ctx->alpha = mdp;
    ctx->alpha *= 1.0 - smoothstep(3,7,mdp_video_fade);
    pos.x += 90 *(1-mdp);

    int len = sizeof(mdp_menu_items)/sizeof(char*);
    int i;
    
    for (i=0;i<len;i++) {
        
        setTexture(ctx, mdp_menu_icon_tex[i]);
        drawImage(ctx, 0, pos.x, pos.y+2, 0);

        setText(ctx,mdp_menu_items[i], 22, 1, 500);
        drawColoredImage(ctx, 0, pos.x+45, pos.y, 0,0.7,0.7,0.7,1);
        pos.y += 55;
    }



}


static void render_selected_item(RenderData *ctx, Pos p, ItemData *selected_item, int is_old) {
    if (!selected_item) return;

    if (is_old) {
        if (out_alpha > 0.2) out_alpha -= 0.01;
        p.a = out_alpha;
    }

    if (!is_old && idle_timeout > 0) {
        idle_timeout--;
        return;
    }
    
    int video_state = 0;
    if ( !is_old && selected_item->video_merch && current_view == GALLERY) {
        if (!(wipe_in >0.05)) {
            video_state = playVideo(selected_item->video_merch);
        }
        if (video_state == 2) {
            if (gallery_video_fade <1)
            gallery_video_fade += 0.025;
        } else {
            gallery_video_fade = 0;
        }
    }


    else if (wipe_in == 0 && current_view == GALLERY) {
        if (hero_cycle-- <= 0) {
            hero_cycle = HERO_TIME;
            selected_item->hero_idx++;
            wipe_in = 1;
        }
    }
    
    int hc = selected_item->hero_count;

    float mdp_fade =  1.0 - lolomo_mode.current*.5;
    float title_fade =  1.0;
    
    struct TextureUrlNode *current_img = selected_item->hero[selected_item->hero_idx%hc];


    if (current_view == MDP) {
        if (playVideo(TESTVIDEO0) == 2 && lolomo_mode.current == 1) {
            mdp_video_fade+=0.015;
            title_fade = smoothstep(7,3,mdp_video_fade);
            ctx->alpha = 1.0 - smoothstep(3,7,mdp_video_fade);
            ctx->alpha *= 1.0 - (smoothstep(1,2,mdp_video_fade)*.2);
            mdp_fade *= smoothstep(2,1,mdp_video_fade);
            drawVideoPlane(ctx, 0,0,1280,720, 1.0-smoothstep(1,2,mdp_video_fade));
        }
    } else {
        mdp_video_fade  = 1;
    }


    if (!is_old) {
        if (wipe_in >0.001) {
            
            if (show_prev_hero) {
                float f = wipe_in-1;
                ctx->alpha = (f*f*f*f*f+1) * p.a * mdp_fade;
                setTexture(ctx,selected_item->hero[(selected_item->hero_idx-1)%hc]);
                drawVin(ctx, 1280-912,0, 912, 513, 0, 0, 1);
            }
            if (current_img->texture) {
                old_selected_item = NULL;
                wipe_in += -wipe_in/wipe_time;
            }

        } else {

            show_prev_hero = 1;
            wipe_in = 0;
            wipe_time = 25;
        }
    }
    ctx->alpha =p.a * mdp_fade;

    setTexture(ctx,current_img);
    drawVin(ctx,1280-912,0, 912, 513, is_old? 0 : wipe_in, (video_state==2)?gallery_video_fade:0.0, (selected_item->video_merch)?0:1);







    if (selected_item->title_img->texture && selected_item->title_alpha < 1) {
        selected_item->title_alpha += 0.03;
    }

    ctx->alpha =selected_item->title_alpha * p.a * title_fade;
    setTexture(ctx,selected_item->title_img);
    drawSizedImage(ctx, 0,p.x,p.y, 0, TITLE_W, TITLE_H);

    p.y += TITLE_H+50;
    setText(ctx,selected_item->desc, 22, 3, TITLE_W*1.3);
    drawColoredImage(ctx, 0, p.x, p.y, 0,0.6,0.6,0.6,1);

}

static void render_item(RenderData *ctx, Pos pos, ItemData *item) {
    if (item->boxshot_alpha < 1 && item->boxshot->texture) {
        item->boxshot_alpha+=0.05;
    } else if (!item->boxshot->texture) {
        item->boxshot_alpha = 0;
    }
    ctx->alpha = pos.a*item->boxshot_alpha;
      if (zoltar_mode.current < 1.0001) {
        return;
    }
    setTexture(ctx, item->boxshot);
    drawGalleryTile(ctx, 0, pos.x, pos.y, 0, pos.w*PADDING, pos.h*PADDING, lolomo_data->position.currentVelocity);
}

static float zoltar_row_position = 0;
static float zoltar_row_alpha = 1;

static void render_row(RenderData *ctx, Pos pos, RowData *rowData) {
    pos.x += 90;
    ctx->alpha = pos.a;
    if (zoltar_mode.current > 1.0001) {
        setText(ctx,rowData->title, 22, 1, 500);
        drawColoredImage(ctx, 0, pos.x, pos.y, 0,0.8,0.8,0.8,1);
    }
    pos.y += 33;
    pos.h -= 35;

    if (rowData->count == 0 || rowData->idx ==0) {
        zoltar_row_position = pos.y;
        zoltar_row_alpha = pos.a;
        return;
    }
   
    update(&rowData->position, ctx->time);
    
    float currentPosition = rowData->position.current;
    unsigned int len = rowData->count;
    int idx = toInt(currentPosition);
    int i = 0;
    float offset = currentPosition - idx;
    
    pos.x -= pos.w;
    pos.x += offset * -pos.w;

    int numTiles = (1280/(int)pos.w)+1;

    for (i=idx-1;i<idx+numTiles;i++) {
        render_item(ctx, pos, rowData->items + modulo(i,len));
        pos.x += pos.w;
    }
}


static float lastKeyTime;
static void render_lolomo(RenderData *ctx, Pos pos, LolomoData *lolomoData) {


    unsigned int len = lolomo_data->count;
    int i;
    float rowstart = 395;

    if (len == 0) return;
    pos.a =1;
    pos.x += 90;
    pos.y += 40;
    
    if (selected_item) {
        render_selected_item(ctx, pos, selected_item,0);
    }
    if (old_selected_item) {
        render_selected_item(ctx, pos, old_selected_item, 1);
    }
    pos.x -= 90;
    pos.y -= 40;
    
    pos.y += rowstart;

    RowData *row = lolomo_data->rows;

    int key = api->NRD_pollkey();

    if (key) {
        if (ctx->time-lastKeyTime < 0.05) {
            key = 0;
        } else {
            lastKeyTime = ctx->time;
        }
        
    } 

    update(&zoltar_scroll,ctx->time);
    update(&lolomo_mode,ctx->time);
    update(&zoltar_mode,ctx->time);
    update(&demo0,ctx->time);
    update(&lolomoData->position, ctx->time);


    if (key == 400) {
        if (demo0.to) {
            animateTo(&demo0, ctx->time,  -1, 0.5);
        } else {
            animateTo(&demo0, ctx->time,  1, 0.5);
        }
    }

    if (key == 401) {
        lolomo_load_status = 5;
    }

    ctx->demo0 = demo0.current;


    if (current_view == ZOLTAR) {
        if (key == 102)      animateTo(&zoltar_scroll, ctx->time,  -1, 0.5);
        else if (key == 100) animateTo(&zoltar_scroll, ctx->time,  1, 0.5);
        
    }
    
    if (key == 13 && selected_item && current_view == GALLERY) {
        animateTo(&lolomo_mode, ctx->time,  1, 0.25);
        current_view = MDP;
    }

    

    if (key == 101) { // up
        if (current_view == ZOLTAR) {

            animateTo(&zoltar_mode, ctx->time,  1, .5);
            if (zoltar_mode.to > 1) {
                animateTo(&zoltar_mode, ctx->time,  .25, .75);
                current_view = GALLERY;
                animateTo(&lolomoData->position, ctx->time,  -1, 0.5);
            }

        } else if (current_view == GALLERY) {
            animateTo(&lolomoData->position, ctx->time, -1, .25);
        }
        
    } else if (key == 103) { //down
        if (current_view == ZOLTAR) {
            animateTo(&zoltar_mode, ctx->time,  1, .5);
            if (zoltar_mode.to > 1) {
                animateTo(&zoltar_mode, ctx->time,  .25, .75);
                current_view = GALLERY;
                animateTo(&lolomoData->position, ctx->time,  1, 0.5);
            }
        } else if (current_view == GALLERY) {
            animateTo(&lolomoData->position, ctx->time,  1, .25);    
        }
    }

  


    //scrollDown += (scrollDownTarget-scrollDown)/5;
    pos.a = 1;
    pos.w = TILE_W;
    pos.h = TILE_H+35;

    float scrollDown = lolomoData->position.current;
    
    int idx = toInt(scrollDown);
    float offset = scrollDown - idx;
    int numTiles = 4;

    pos.y += offset * -(pos.h);
    pos.y -= (pos.h);




    pos.x -= 100*lolomo_mode.current;

    float startAlpha = pos.a * 1.0-lolomo_mode.current;

    
    for (i=idx-1;i<idx+numTiles;i++)  {

        float a = startAlpha;
        
        RowData *row = lolomoData->rows + modulo(i,len);
    
        if (pos.y < rowstart)
            a *= (pos.y - (rowstart - pos.h)) / pos.h;

        else if (pos.y > rowstart + pos.h*0.5)
            a *= 1 - ((pos.y - (rowstart + pos.h*0.5)) / (pos.h*3.5));

        else
            a *= 1.0;

        if (pos.y > 720) {
            a *= 0;
        }
        
        pos.a = a;
        render_row(ctx, pos, row);


        if (current_view == GALLERY &&  pos.y >= rowstart-100 && pos.y < rowstart+100) {
            if (key == 102) {
                animateTo(&row->position, ctx->time,  1, 0.25);
                
            }
            else if (key == 100) {
                animateTo(&row->position, ctx->time,  -1, 0.25);
            }
        }

        pos.y += pos.h;


    }

    

    if (key == 100 && current_view == MDP) {
        animateTo(&lolomo_mode, ctx->time,  -1, 0.25);
        current_view = GALLERY;
    }
    
    if (current_view == GALLERY)
    drawHighlight(ctx, pos.x+90, rowstart+33, TILE_W*PADDING, TILE_H*PADDING);

    selected_row = lolomoData->rows + modulo(lolomoData->position.to,len);
    ItemData *current_item = selected_item;
    ItemData *new_item = selected_row->items + modulo(selected_row->position.to,selected_row->count);
    
    if (current_item != new_item) {
        new_item->title_alpha  =0;
        if (idle_timeout <1) {
            out_alpha = 1;
            old_selected_item = selected_item;
        }
        selected_item = new_item;
        selected_item->hero_idx = 0;
        hero_cycle = HERO_TIME;
        wipe_in = 1;
        show_prev_hero = 0;
        wipe_time = 8;
        idle_timeout = 20;
    }
    if (selected_row->idx == 0) {
        selected_item = NULL;
        old_selected_item = NULL;
        if (current_view == GALLERY) {
            animateTo(&zoltar_mode, ctx->time, -1.25, .6);
            current_view = ZOLTAR;
        }
    }



    pos.y = rowstart;
    pos.x = 95;
    render_mdp(ctx, pos);

    ctx->alpha = 1.0 - lolomo_mode.current;
    //200/54
    setTexture(ctx, tex_logo);
    drawSizedImage(ctx, 0, 1280-225,720-75,0, 200*0.69, 52*0.69);



}




#define sliver_size 116
#define sliver_size_present 256

static int loading_zoltar = 1;
static int zoltar_pause = 15;
static float zoltar_video_fade_alpha = 0;

static void render_zoltar_img(RenderData *ctx, int idx, ItemData *data, float scroll, float interest) {

    if (zoltar_row_alpha < 0.001) return;

    float base = floor(scroll);
    float ext = scroll - base;
    scroll = base + smoothstep(0, 1,ext);

    // Zoltar Image
    float base_height = 720  + ((TILE_H - 720)*zoltar_boxmode);
    float base_width = sliver_size + (1-zoltar_navmode)*(sliver_size_present-sliver_size);

    base_width += (TILE_W - base_width) * zoltar_boxmode;

    float expand_by = (1280-sliver_size*3) * zoltar_navmode;

    float padding = PADDING + (1-PADDING)* (1-zoltar_boxmode);

    float tail = clamp(scroll, 0, 1);
    float present = 1-zoltar_navmode;
    float show_interest = clamp(tail+present, 0, 1);
    float sliver = zoltar_navmode * tail;
    float expand = expand_by * tail;

    float w = (expand_by+base_width) - expand;
    float x = -65*zoltar_navmode + scroll * base_width + expand + clamp(scroll, -1, 0) * expand_by;
    x += 90 *zoltar_boxmode;


    if (data->boxshot_alpha < 1 && data->hero[0]->texture) {
        data->boxshot_alpha+=0.033;
        loading_zoltar = 1;
    } else if (!data->hero[0]->texture || !data->title_img->texture) {
        data->boxshot_alpha = 0;
        loading_zoltar = 1;
    }

    float brightness = 1.0 - sliver*0.5;
    brightness *= data->boxshot_alpha;
    ctx->alpha = zoltar_row_alpha;

    if (data->video_merch && idx == 0 && !zoltar_scroll.currentVelocity && zoltar_navmode==1 && playVideo(data->video_merch) == 2){
        ctx->alpha = zoltar_video_fade_alpha;
        zoltar_video_fade_alpha -= .025;
    } else if (idx == 0) {
        //printf("%f %i\n", zoltar_video_fade_alpha, playVideo(data->video_merch));
        zoltar_video_fade_alpha = 1;
    }

    setTexture(ctx, data->hero[0]);
    drawZoltarImage(ctx, x ,zoltar_row_position*zoltar_boxmode,w*padding, base_height*padding, interest*(show_interest), zoltar_navmode, 1-zoltar_boxmode, brightness);
    ctx->alpha = zoltar_row_alpha;
    // Logo
    float logo_scale = .9 - zoltar_boxmode*.5 - present*.4 - sliver*.4;
    float title_y = 450 + 50*(sliver+present);
    float title_x = 150 - 170*sliver - present*125;

    ctx->alpha *= (1-zoltar_boxmode) * data->boxshot_alpha;
    setTexture(ctx, data->title_img);
    drawSizedImage(ctx, 0, x+title_x ,title_y,0,TITLE_W*logo_scale, TITLE_H*logo_scale);
}

static float interest[] = {0.3, 0.2, 0.05, 0.37, 0.03, -0.1, 0.08, -0.05, -0.26, 0.07, -0.00};

static void render_zoltar(RenderData *ctx, Pos pos, LolomoData *lolomoData) {

    zoltar_navmode = smoothstep(0,1,zoltar_mode.current)- smoothstep(1,2,zoltar_mode.current);
    zoltar_boxmode = smoothstep(1,2,zoltar_mode.current);

    float s = zoltar_scroll.current;

    int extra = (lolomo_load_status ==2) ? 0 : 1;

    int idx = toInt(-s)-extra;
    int len = idx+5+extra;
    
    for (;idx<len;idx++) {
        int i = modulo(idx,5);
        render_zoltar_img(ctx, s+idx, &lolomoData->rows[0].items[i], idx+s, interest[i]);        
    }    
}



char mem_str[30];

#ifdef __ROKU__
#include <unistd.h>
#include <stdio.h>

#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void memlog( ) {
    long rss = 0L;
    FILE* fp = NULL;
    if ( (fp = fopen( "/proc/self/statm", "r" )) == NULL ) {
        //return (size_t)0L;      /* Can't open? */
    }
    if ( fscanf( fp, "%*s%ld", &rss ) != 1 )
    {
        fclose( fp );
        //return (size_t)0L;      /* Can't read? */
    }
    fclose( fp );

    sprintf(mem_str, "Live RSS: %zu bytes", (size_t)rss * (size_t)sysconf( _SC_PAGESIZE));
    printf("memlogmemlogmemlogmemlog:: %zu\n", (size_t)rss * (size_t)sysconf( _SC_PAGESIZE));

}
#else
void memlog( ) {}

#endif

extern void draw_screensaver(RenderData *ctx);

void render_loop(RenderData *ctx) {

    Pos pos = { .x = 0.0, .y = 0.0 };
    frame_uses_video = 0;

    draw_screensaver(ctx);
    return;

    switch (lolomo_load_status) {
        case 0:
            memset(&mem_str, 0, 30);
            curl_json(LOLOMO_JSON_URL, NULL, lolomo_json_callback);
            init_icons();
            lolomo_load_status++;
        case 1: return;
        case 2:
            if (loading_zoltar) {
                loading_zoltar = 0;
            } else {
                lolomo_load_status++;
            }
            render_zoltar(ctx, pos, lolomo_data);
            break;
        case 3:
            if (zoltar_pause-- <= 0) {
                animateTo(&zoltar_mode, ctx->time,  1, .5);
                lolomo_load_status++;
            }
            render_zoltar(ctx, pos, lolomo_data);
            break;
        case 4:
            //api->NRD_close_video();
            render_lolomo(ctx, pos, lolomo_data);
            render_zoltar(ctx, pos, lolomo_data);
            //drawVideo(ctx, 0,0,1280,720);
            break;
         case 5:
            memlog();
            lolomo_load_status++;
         case 6:
            ctx->alpha = 1.0;
            setTexture(ctx, thanku);
            drawImage(ctx, 0,0,100,0);
            setTexture(ctx, tex_logo);
            drawSizedImage(ctx, 0, 1280-225,720-75,0, 200*0.69, 52*0.69);

            setText(ctx,&mem_str[0], 40, 1, 1000);
            drawColoredImage(ctx, 0, 50, 50, 0,0.7,0.7,0.7,1);

            break;

    }

    if (frame_uses_video == 0) {
        stopVideo();
    }


}
