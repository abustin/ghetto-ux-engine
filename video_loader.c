#include "curl/curl.h"
#include "NRD_HAL.h"

void network_streamer(char* url, curl_write_callback write_cb, curl_finish_callback finish_cb, curl_progress_callback prog_cb, void *userdata) {

    CURL *curl_handle = curl_easy_init();
    CURLcode res;

    curl_easy_setopt(curl_handle, CURLOPT_NOSIGNAL, 1);
    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYHOST, 0);
    if (write_cb) curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_cb);
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "agent-bustin/1.0");
    curl_easy_setopt(curl_handle, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    if (prog_cb) curl_easy_setopt(curl_handle, CURLOPT_PROGRESSFUNCTION, prog_cb);
    curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 0);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, userdata);
    curl_easy_setopt(curl_handle, CURLOPT_URL, url);
    res = curl_easy_perform(curl_handle);

}



