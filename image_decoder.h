#ifndef _image_decoder_h_
#define _image_decoder_h_

struct Mem {
	void *mem;
	int pos;
	int size;
};

struct Bitmap {
	int width, height, channels, pitch;
	void *pixels;
};


struct Bitmap *decodeIMG(void *mem, int size);

#endif