#include <stdio.h>

#include <dlfcn.h>
#include <time.h>
#include "GLES.h"
#include "texture_loader.h"
#include "NRD_HAL.h"
#include "render.h"
#include "video_streamer.h"
#include <pthread.h>
#include <sched.h>


#include <sys/types.h>
#include <sys/stat.h>

#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>



void handler(int sig) {
        void *array[10];
        size_t size;

        //get void*'s for all entries on the stack
        size = backtrace(array, 10);
        // // print out all the frames to stderr
        fprintf(stderr, "Error: signal %d:\n", sig);
        backtrace_symbols_fd(array, size, STDERR_FILENO);
        exit(1);
}


time_t get_mtime(const char *path)
{
    struct stat statbuf;
    if (stat(path, &statbuf) == -1) {
        return 1;
    }
    return statbuf.st_mtime;
}


static void *portable_handle;
static NRDHAL base_api;
static time_t lib_mod_time;
static int lib_mod_check = 0;

void load_portable() {

    if (lib_mod_check == 0) {

        lib_mod_check = 120;

        time_t mod_time = 1;//get_mtime("./libportable.so");


        if (mod_time > lib_mod_time) {

            if (portable_handle) {

                void (*end_module)();
                *(void **) (&end_module) = dlsym(portable_handle, "NRD_end_module");
                end_module(&base_api);
                dlclose(portable_handle);
            }

            lib_mod_time = mod_time;

            #ifdef __ROKU__
            void *handle = dlopen("pkg_/libportable.so", RTLD_LAZY);
            #else
            void *handle = dlopen("@executable_path/libportable.so", RTLD_LAZY);
            #endif
            portable_handle = handle;

            if (!handle) printf("%s\n", dlerror());
            dlerror();
            
            void (*start_module)();
            *(void **) (&start_module) = dlsym(handle, "NRD_start_module");

            if (start_module) {
                start_module(&base_api);
            } else {
                printf("%s\n", dlerror());
            }

            printf("%p\n", start_module);

        }

    } else {
        lib_mod_check--;

    } 
    
}
#include <pthread.h>
void mainLoop() {
   //load_portable();
   base_api.loop();
   finishFrame();
}

static void startLoop() {
    while(1){ mainLoop();}
}

void NRD_timeout(unsigned int usec) {
    usleep(usec);
}

void NRD_thread_join(NRDThread thread) {
    pthread_join(*((pthread_t*)thread.thread),NULL);
}

NRDThread NRD_thread(void (*thread_main)(void *ptr), void *td) {
    pthread_t* thread = malloc(sizeof(pthread_t));
    pthread_create(thread, NULL, (void *) thread_main, td);
    NRDThread t = { .thread = thread };
    return t;
}
static int video_window[4] = {0,0,1280,720};

static int player_play(char *url) {
    play_video(&base_api, url, NULL);
    set_video_window_size(video_window[0],video_window[1],video_window[2],video_window[3]);
    return 0;
}

static int player_close() {
    stop_video();
    //play_video(&base_api, url, play_it_again);
    return 0;
}

static int set_video_window(int x, int y, int w, int h) {
    //play_video(&base_api, url, play_it_again);
    video_window[0] = x;
    video_window[1] = y;
    video_window[2] = w;
    video_window[3] = h;
    set_video_window_size(x,y,w,h);
    return 0;
}

static int get_video_window(int* x, int* y, int* w, int* h) {
    //play_video(&base_api, url, play_it_again);
    *x = video_window[0];
    *y = video_window[1];
    *w = video_window[2];
    *h = video_window[3];
    return 0;
}

void* NRD_global_sym(char* sym_name) {
    void *f = dlsym(RTLD_NEXT, sym_name);
    if (f == NULL) {
        f = dlsym(RTLD_DEFAULT, sym_name);
    }
    //printf("%20s\t= %p\n", sym_name, f);
//    printf("%p", curl_easy_init);
    return f;

}
#ifdef __ROKU__
#include "roku/bni.h"
#endif

int randkey() {
    return rand()%250;
}



int main(int argc, char** argv) {

#ifdef __ROKU__
    setbuf(stdout,NULL);
    RokuBNI_init();
#endif

    printf("hello world\n");

    

    srand(time(NULL));
    signal(SIGSEGV, handler);   // install our handler 
    #ifdef _GLUT
        glutInit(&argc, argv);
    #endif

    //initText();

    base_api.NRD_close_video = player_close;
    base_api.NRD_play_video = player_play;
    base_api.NRD_set_video_window = set_video_window;
    base_api.NRD_get_video_window = get_video_window;
    base_api.NRD_get_video_state = get_video_state;

    base_api.NRD_digest_frame = digest_frame;
    base_api.NRD_timeout = NRD_timeout;
    base_api.NRD_thread = NRD_thread;
    base_api.NRD_thread_join = NRD_thread_join;
    base_api.NRD_global_sym = NRD_global_sym;
    base_api.NRD_decodeIMG  = decodeIMG;
    //base_api.NRD_createText = createText;
    base_api.NRD_rand = rand;
    base_api.NRD_pollkey = get_last_key;

    printf("hello world 22\n");

    setupWindow(1280,720);

    init_streamer();


        printf("hello world 3\n");


    load_portable();

    printf("hello world 4\n");
    
    #ifdef _GLUT
        glutMainLoop();
    #else
        startLoop();
    #endif
    
    dlclose(portable_handle);

#ifdef __ROKU__
          RokuBNI_fini();
#endif

    return 0;
}



