#ifndef _VIDEO_STREAMER
#define _VIDEO_STREAMER

void set_video_window_size(int x,int y,int w,int h);
int digest_frame(unsigned char*** pixels, int** stride);
//void play_video(char *url);
void play_video(NRDHAL *api, void *url, void (*complete)());
void init_streamer();
void stop_video();
int get_video_state();

#endif