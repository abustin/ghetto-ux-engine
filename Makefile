OBJ = bootstrap.o  render_boot.o image_decoder.o video_streamer.o lib/libav/libavformat/libavformat.a lib/libav/libavcodec/libavcodec.a lib/libav/libavutil/libavutil.a lib/zlib/libz.a
PORTABLES = screensaver.c text_loader.c render.c portable.c render_program.c async_job_queue.c data_loader.c network_streamer.c texture_loader.c cJSON.c lolomo_sample.c  lib/freetype/objs/.libs/libfreetype.a lib/curl/lib/.libs/libcurl.a lib/zlib/libz.a
INCLUDES = -I./lib/freetype/include -I./lib/curl/include -I./lib/libav

ifeq ($(MAKECMDGOALS), mips)
include /usr/local/mipsel-bcm7584/netflix/nexus/97584/a0/platform_app.inc
    OBJ += nexus_common/init.o nexus_common/esutil.o
	CC =/usr/local/mipsel-bcm7584/bin/mipsel-linux-gcc 
    CC_BASE =/usr/local/mipsel-bcm7584/bin/mipsel-linux-gcc
	INCLUDES +=-I. -I./nexus_common  -I/usr/local/mipsel-bcm7584/netflix/include/png -I/usr/local/mipsel-bcm7584/netflix/include  -I/usr/local/mipsel-bcm7584/netflix/include/freetype -I/usr/local/mipsel-bcm7584/netflix/nexus/97584/a0 -I/usr/local/mipsel-bcm7584/netflix/include/curl
    HOST=mipsel-linux-uclibc 
	CFLAGS= $(NEXUS_CFLAGS) -std=c99 -D__NEXUS__ -DTARGET_POSIX -D_LINUX -fPIC -D_GNU_SOURCE
	LDFLAGS=-L/usr/local/mipsel-bcm7584/netflix/lib -lnxclient -lrt -lv3ddriver -lnxpl -lnexus_client -lnexus -lz -lpng -ljpeg   -lm -ldl -pthread -lfreetype 
else ifeq ($(MAKECMDGOALS),linux)
    CC_BASE = gcc
	AR=/home/alex/Downloads/x86_64-linux-musl/bin/x86_64-linux-musl-ar
	CC = /home/alex/Downloads/x86_64-linux-musl/bin/x86_64-linux-musl-gcc 
	CC = gcc
	INCLUDES +=-I. -I./lib/freetype/include
	CFLAGS= -DLIBPNG_VERSION_12 -DTARGET_POSIX -D_LINUX -fPIC -D_GNU_SOURCE -D_GLUT
	LDFLAGS= -lglut -lpng -ljpeg -lGLESv2 -lEGL -lm -ldl -pthread 
	HOST=x86_64-linux
else ifeq ($(MAKECMDGOALS),libportable.mips.so)
    ACC=/home/alex/x-tools/mipsel-unknown-linux-uclibc/bin/mipsel-unknown-linux-uclibc-gcc
    CC=/Volumes/toolchain-builder/x-tools/mipsel-unknown-linux-uclibc/bin/mipsel-unknown-linux-uclibc-gcc
	HOST=mipsel-linux-uclibc
else ifeq ($(MAKECMDGOALS),libportable.pi.so)
    CC=/home/alex/Downloads/arm-linux-musleabihf/bin/arm-musl-linuxeabihf-gcc
    HOST=arm-linux-eabi
else ifeq ($(MAKECMDGOALS),pi)
	CC = gcc
	INCLUDES +=-I. -I/opt/vc/include/ -I/opt/vc/include/interface/vcos/pthreads -I/opt/vc/include/interface/vmcs_host/linux
	CFLAGS=-g -DSTANDALONE -D__STDC_CONSTANT_MACROS -D__STDC_LIMIT_MACROS -DTARGET_POSIX -D_LINUX -fPIC -DPIC -D_REENTRANT -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -U_FORTIFY_SOURCE -Wall -g -DHAVE_LIBOPENMAX=2 -DOMX -DOMX_SKIP64BIT -ftree-vectorize -pipe -DUSE_EXTERNAL_OMX -DHAVE_LIBBCM_HOST -DUSE_EXTERNAL_LIBBCM_HOST -DUSE_VCHIQ_ARM -Wno-psabi -D_GNU_SOURCE -D__PI__
	LDFLAGS=-L/opt/vc/lib/  -lpng -ljpeg -lGLESv2 -lEGL -lopenmaxil -lbcm_host -lvcos -lvchiq_arm -lpthread -lrt -lm -ldl
else ifeq ($(MAKECMDGOALS),roku)
		AV_CONF=--enable-cross-compile --target-os=linux --arch=arm --extra-cflags=" -mcpu=arm1176jzf-s -mfloat-abi=softfp -mfpu=vfp" --disable-extra-warnings
	CC =/usr/local/arm-roku-giga/bin/arm-brcm-linux-gnueabi-gcc
    CC_BASE =/usr/local/arm-roku-giga/bin/arm-brcm-linux-gnueabi-gcc
    HOST=arm-none-linux-gnueabi
	INCLUDES+=-I/usr/local/arm-roku-giga/roku/usr/include -I/usr/local/arm-roku-giga/netflix/include
	CFLAGS= -DTARGET_POSIX -D_LINUX -fPIC -Wno-psabi -D_GNU_SOURCE -D__ROKU__ -mcpu=arm1176jzf-s
	LDFLAGS=-L/usr/local/arm-roku-giga/netflix/lib -L/usr/local/arm-roku-giga/roku/usr/lib -lpng -ljpeg -lGLESv2 -lEGL -lpthread -lrt -lm -ldl -lz -lRokuNDK -lRokuNDKAV -lRokuNdkDrm -mcpu=arm1176jzf-s
else
	CC = gcc
	CC_BASE = gcc
	INCLUDES +=-I. -I/System/Library/Frameworks/GLUT.framework/Headers 
	CFLAGS= -D_GLUT -ffast-math -fno-stack-protector -D_FORTIFY_SOURCE=0
	LDFLAGS=-ldl -framework GLUT -framework OpenGL -lpng -ljpeg -lpthread
	HOST=x86_64-apple-darwin
endif


all: app libportable.so

linux: all
roku: all
mips: app libportable.mips.so
pi: all

roku.bin: roku
	cp -R cramfs.tmp cramfs.dir
	mkdir -p cramfs.dir/bin
	cp app cramfs.dir/app
	cp libportable.so cramfs.dir/libportable.so
	cp Arial_for_Netflix-B.ttf cramfs.dir/Arial_for_Netflix-B.ttf
	mkdir -p cramfs.dir/bin
	cd cramfs.dir/bin && ln -sF ../app
	cd cramfs.dir/bin && ln -sF ../libportable.so
	chmod 775 cramfs.dir/app
	chmod 775 cramfs.dir/libportable.so
	/usr/local/arm-roku-giga/bin/mkcramfs_roku cramfs.dir roku.bin

# -fexceptions -rdynamic 

app: $(OBJ)
	$(CC_BASE) -Ofast $(LDFLAGS) $^ -o app 

libs: libportable.so libportable.mips.so libportable.pi.so

libportable.so: $(PORTABLES) shaders/*.vert shaders/*.frag
	$(CC) -Ofast $(CFLAGS) $(INCLUDES) -D_PORTABLE -I. -fPIC -shared -L/usr/local/arm-roku-giga/roku/usr/lib $(PORTABLES)  -o libportable.so 

libportable.mips.so: $(PORTABLES)
	$(CC) -fexceptions -rdynamic -g -Os -D_PORTABLE  $(INCLUDES) -mips32 -mhard-float  -shared -fPIC  $(PORTABLES)  -o libportable.mips.so

libportable.pi.so: $(PORTABLES)
	$(CC) -Os -fPIC $(INCLUDES) -D_PORTABLE -mcpu=cortex-a7 -mfpu=neon-vfpv4 -mfloat-abi=hard -shared  $(PORTABLES)  -o libportable.pi.so 

%.o: %.c
	$(CC_BASE) -Ofast $(CFLAGS) -D_GL_DEFINES $(INCLUDES) -c -ldl -o $@ $<	


lib/._placeholder:
	mkdir lib
	touch lib/._placeholder

lib/zlib/._placeholder: lib/._placeholder
	wget http://zlib.net/zlib-1.2.8.tar.gz
	tar -xf zlib-1.2.8.tar.gz -C lib
	mv lib/zlib-1.2.8 lib/zlib
	rm zlib-1.2.8.tar.gz
	touch lib/zlib/._placeholder

lib/zlib/libz.a: lib/zlib/._placeholder
	cd lib/zlib && \
		CC=$(CC) ./configure --static && make -j


lib/curl/._placeholder: lib/._placeholder
	wget http://curl.haxx.se/download/curl-7.45.0.tar.gz
	tar -xf curl-7.45.0.tar.gz -C lib
	mv lib/curl-7.45.0 lib/curl
	rm curl-7.45.0.tar.gz
	touch lib/curl/._placeholder

lib/curl/lib/.libs/libcurl.a: lib/curl/._placeholder
	cd lib/curl && \
		CC=$(CC) ./configure  --disable-shared --without-ssl --disable-ldap --without-librtmp --disable-manual -disable-librtsp  --without-libssh2 -disable-ftp -disable-dict -disable-gopher -disable-imap -disable-pop3 -disable-rtsp -disable-telnet -disable-tftp -disable-smtp --host=$(HOST) && make -j

lib/freetype/._placeholder: lib/._placeholder
	wget http://download.savannah.gnu.org/releases/freetype/freetype-2.6.1.tar.gz
	tar -xf freetype-2.6.1.tar.gz -C lib
	rm freetype-2.6.1.tar.gz
	mv lib/freetype-2.6.1 lib/freetype
	touch lib/freetype/._placeholder

lib/freetype/objs/.libs/libfreetype.a: lib/freetype/._placeholder
	cd lib/freetype && \
		CC=$(CC) ./configure --without-zlib --without-bzip2 --without-harfbuzz --without-png --with-pic --host=$(HOST) && make -j
	cd ../..

lib/libav/libavcodec/libavcodec.a: lib/libav/config.h
lib/libav/libavutil/libavutil.a: lib/libav/config.h
lib/libav/libavformat/libavformat.a: lib/libav/config.h

lib/libav/._placeholder: lib/._placeholder
	wget https://libav.org/releases/libav-11.4.tar.xz
	tar -xf libav-11.4.tar.xz -C lib
	rm libav-11.4.tar.xz
	mv lib/libav-11.4 lib/libav
	touch lib/libav/._placeholder


lib/libav/config.h: lib/libav/._placeholder
	cd lib/libav && \
	CC=$(CC) ./configure --cc=$(CC) $(AV_CONF) --disable-everything --enable-runtime-cpudetect  --enable-gpl --enable-nonfree --enable-static --enable-avfilter --enable-parser=mpeg4video --enable-parser=mpegaudio --enable-parser=mpegvideo --enable-parser=mpeg4video --enable-demuxer=m4v --enable-demuxer=mpegts --enable-parser=mov --enable-demuxer=avi --enable-demuxer=mov --enable-decoder=h264 --enable-parser=h264 --enable-demuxer=h264 --enable-decoder=aac --enable-parser=aac --enable-demuxer=aac --enable-decoder=mp3 --enable-demuxer=mp3 --enable-pic --enable-pthreads --enable-network --disable-programs --enable-protocol=http && make

clean:
	rm app
	rm *.o
	rm *.so
	rm -rf lib
