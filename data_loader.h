#ifndef _data_loader
#define _data_loader




#define INIT_MEM_SIZE 1024*512

#include "async_job_queue.h"



void data_loader_boot();
void wake_loader();
void data_loader_shutdown();

#endif
