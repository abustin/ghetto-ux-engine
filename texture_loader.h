#ifndef _texture_loader_h_
#define _texture_loader_h_

#include "NRD_HAL.h"
#include "data_loader.h"
#include "render_node.h"
#include <stdint.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

#define BUCKET_SIZE 31

typedef struct TextureUrlNode {
    char* url;
    unsigned int texture;
    unsigned int active;
    unsigned int last_used;
    int width;
    int height;
    int type;
    int size, lines, wrapWidth;
    uint32_t hash;
    struct TextureUrlNode *lru_next;
    struct TextureUrlNode *lru_prev;
    struct TextureUrlNode *next;
} TextureUrlNode;



struct TextFormat {
    unsigned int color;
    unsigned int size;
};

typedef struct Loader {
    int load_id;
	int width;
    int height;
    int ori_width;
    int ori_height;
    unsigned int texture;
    char* url;
    const char* type;
    void (*callback)(struct Loader *loader);
    void *context;
    
} Loader;

typedef void (*ImageLoadingHandler)(struct Loader *loader);

//char abs_exe_path[PATH_MAX];
int should_cancel_texture_load(Loader *loader);
void getImageSize(float *width, float *height);
void setTexture(struct RenderData* rd, struct TextureUrlNode* record);
void commitTexture(struct RenderData* rd);
unsigned int purgeLru(int w, int h);
struct TextureUrlNode* getTexture(char* url);
void setImage(char* url);
void setText(struct RenderData* rd, char* text, int size, int lines, int wrapWidth);
void purge_texture_cache();


#endif