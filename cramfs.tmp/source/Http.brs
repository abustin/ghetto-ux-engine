REM ******************************************************
REM Copyright Roku 2011,2012,2013.
REM All Rights Reserved
REM ******************************************************

REM If you import this, you also need to import Sort, Types, and StringUtils.
REM

REM ******************************************************
REM
REM Http Query builder
REM
REM provides structured access to URL parameters and
REM various sync and async HTTP access methods
REM
REM tracks multiple simultaneously active requests
REM and maps responses to requests
REM
REM The oauth protocol needs to interact with parameters in a
REM particular way, so access to groups of parameters and
REM their encodings are provided as well.
REM
REM compatible with Dispatcher.brs
REM
REM Several callbacks can be placed on the returned http object
REM by the calling code to be called by this code when appropriate:
REM callbackPrep - called right before sending
REM callbackReceive - called right after receiving a response
REM callbackRetry - called after failure if retries>0
REM callbackCancel - called after failure if retries=0
REM retainBodyOnError - set to true if body is needed even on errors
REM These allow side effects without explicitly coding them here.
REM
REM Users of this class should implement their own version of
REM appLog(). The http class invokes this to let the client
REM code to perform http logging.
REM ******************************************************

Function HttpClass() As Object
    ' singleton class def
    this = m.ClassHttp
    if this=invalid
        this = CreateObject("roAssociativeArray")

        'computed accessors
        this.Parse                     = http_parse
        this.AddParam                  = http_add_param
        this.AddAllParams              = http_add_all_param
        this.RemoveParam               = http_remove_param
        this.GetURL                    = http_get_url
        this.GetParams                 = http_get_params
        this.GetCookies                = http_get_cookies
        this.GetRequestHeaders         = http_get_request_headers
        this.GetResponseHeaders        = http_get_response_headers
        this.ParamGroup                = http_get_param_group
        this.Header                    = http_header

        'sync transfers
        this.GetToStringWithRetry      = http_get_to_string_with_retry
        this.GetToStringWithTimeout    = http_get_to_string_with_timeout
        this.PostFromStringWithRetry   = http_post_from_string_with_retry
        this.PostFromStringWithTimeout = http_post_from_string_with_timeout
        this.AsyncPostFromString       = http_async_post_from_string

        this.PutFromStringWithTimeout     = http_put_from_string_with_timeout
        this.DeleteFromStringWithTimeout  = http_delete_from_string_with_timeout

        'async control
        this.Go                        = http_go
        this.Ok                        = http_ok
        this.Sync                      = http_sync
        this.Receive                   = http_receive
        this.Cancel                    = http_cancel
        this.CheckTimeout              = http_check_timeout
        this.Retry                     = http_retry

        'internal
        this.Prep                      = http_prep
        this.Wait                      = http_wait_with_timeout
        this.Dump                      = http_dump

        'logging
        this.logRequestResponse        = http_log_request_response
        if type(appLog) = "Function"
            this.appLog                = appLog
        else
            print "Http: WARNING -- appLog() is undefined by the current channel."
            print "Http: WARNING -- appLog() needs to be defined to upload script logs."
            print "Http: WARNING -- using default non-uploading appLog() implementation."
            this.appLog                = http_default_app_log
        end if
        if type(sanitizeUrl) = "Function"
            this.sanitizeUrl           = sanitizeUrl
        else
            print "Http: WARNING -- sanitizeUrl() is undefined by the current channel."
            print "Http: WARNING -- sanitizeUrl() needs to be defined to sanitize urls before logging."
            print "Http: WARNING -- using default sanitizeUrl() implementation."
            this.sanitizeUrl           = http_default_sanitize_url
        end if


        this.retainBodyOnError         = false

        m.ClassHttp = this
    end if
    return this
End Function

Function HttpContext(key=invalid As Dynamic, value=invalid As Dynamic) As Dynamic
    ' used to store variations to apply globally
    hc = m.HttpContext
    if hc=invalid
        hc = CreateObject("roAssociativeArray")
        m.HttpContext = hc
    end if
    if key<>invalid
        if value<>invalid then hc[key] = value
        return hc[key]
    end if
    return hc
End Function

Function NewHttp(url As String, port=invalid As Dynamic, method="GET" As String, certificatesFile="" As String) as Object
    this = CreateObject("roAssociativeArray")
    this.append(HttpClass())

    this.port    = port 
    this.method  = method
    this.anchor  = ""
    this.label   = "init"
    this.status  = 0
    this.timeout = 15000 ' 15 secs
    this.retries = 1
    this.timer   = CreateObject("roTimespan")
    this.certificatesFile = certificatesFile

    this.append(HttpContext())
    headers = this.headers 
    this.headers = CreateObject("roAssociativeArray")
    if headers<>invalid then this.headers.append(headers)

    this.Parse(url)

    return this
End Function

' convenience factories

Function NewHttpXML(url As String, port=invalid As Dynamic, method="GET" As String) as Object
    http = NewHttp(url, port, method)
    http.header("Content-Type","text/xml")
    return http
End Function

Function NewHttpForm(url As String, port=invalid As Dynamic, method="GET" As String) as Object
    http = NewHttp(url, port, method)
    http.header("Content-Type","application/x-www-form-urlencoded")
    return http
End Function

Function NewHttpPlain(url As String, port=invalid As Dynamic, method="GET" As String) as Object
    http = NewHttp(url, port, method)
    http.header("Expect","")
    return http
End Function

Function NewHttpWithRespProcessing(url As String, port=invalid As Dynamic, method="GET" As String, certificatesFile="" As String, postProcessHttpResponse=invalid As Dynamic, replaceHost="" As String, sslOnly=false As Boolean) as Object
    if replaceHost <> "" then
        url = replace_host_domain(url, replaceHost)
    end If

    if sslOnly then
            'Filter url. convert http://to https://
            protocolLength = len("http://")
            if left(url, protocolLength) = "http://" then
                    url = "https://" +  Mid(url, protocolLength + 1)
            endif
    endif

    http = NewHttp(url, port, method, certificatesFile)
    http.retries = 2 
    http.callbackReceive = postProcessHttpResponse
    http.retainBodyOnError = true
    return http
End Function

Function replace_host_domain(url As String, host As String) As String
    colonPos = Instr(1, url, "://")
    if (colonPos > 0)
        slashPos = Instr(colonPos + 3, url, "/")
        len = Len(url)
        if (slashPos = 0)
            url = Left(url, colonPos + 2) + host
        else
            url = Left(url, colonPos + 2) + host + Right(url, len - slashPos + 1)
        end if
    end if

    return url
End Function

Function http_firmware_atleast_four() As Boolean
    version = CreateObject("roDeviceInfo").GetVersion()

'    print("Firmware Version Found: " + version)

    major = Mid(version, 3, 1)
    minor = Mid(version, 5, 2)
    build = Mid(version, 8, 5)

'    print("Major Version: " + major + " Minor Version: " + minor + " Build Number: " + build)

    if Val(major) < 4 then
        return false
    end if
    return true
End Function


REM ******************************************************
REM
REM Setup the underlying http transfer object.
REM
REM ******************************************************

Function http_prep(method="" As String)
    ' this callback allows just-before-send
    ' mods to the request, e.g. timestamp
    if isfunc(m.callbackPrep) then m.callbackPrep()
    m.status  = 0
    m.response = invalid 'set to a roUrlEvent in http_receive()
    urlobj = CreateObject("roUrlTransfer")
    if type(m.port)<>"roMessagePort" then m.port = CreateObject("roMessagePort")
    urlobj.SetPort(m.port)
    urlobj.EnableEncodings(True)
    if http_firmware_atleast_four() then
            urlobj.RetainBodyOnError(m.retainBodyOnError)
    end if
    url = m.GetUrl()
    urlobj.SetUrl(url)
    if method<>"" then m.method = method else method = m.method
    urlobj.SetRequest(method)
    headers = m.headers
    if type(headers)="roAssociativeArray"
        for each key in headers
            val = validstr(headers[key])
            urlobj.addHeader(key,val)
        end for
    end if
    certFile = m.CertificatesFile
    if isnonemptystr(certFile)
        urlobj.SetCertificatesFile(certFile)
        urlobj.InitClientCertificates()
    end if
    HttpActive().replace(m,urlobj)
    m.timer.mark()
End Function

REM ******************************************************
REM
REM Parse an url string into components of this object
REM
REM ******************************************************

Function http_parse(url As String) as Void
    remnant = CreateObject("roString")
    remnant.SetString(url)

    anchorBegin = Instr(1, remnant, "#")
    if anchorBegin>0
        if anchorBegin<Len(remnant) then m.anchor = Mid(remnant,anchorBegin+1)
        remnant = Left(remnant,anchorbegin-1)
    end if

    paramBegin = Instr(1, remnant, "?")
    if paramBegin > 0
        if paramBegin < Len(remnant) then m.GetParams("urlParams").parse(Mid(remnant,paramBegin+1))
        remnant = Left(remnant,paramBegin-1)
    end if

    m.base = remnant
End Function

REM ******************************************************
REM
REM Add an URL parameter to this object
REM
REM ******************************************************

Function http_add_param(name As String, val As String, group="" As String)
    params = m.GetParams(group)
    params.add(name,val)
End Function


Function http_add_all_param(keys as object, vals as object, group="" As String)
    params = m.GetParams(group)
    params.addall(keys,vals)
End Function

REM ******************************************************
REM
REM Remove an URL parameter from this object
REM
REM ******************************************************

Function http_remove_param(name As String, group="" As String)
    params = m.GetParams(group)
    params.remove(name)
End Function

REM ******************************************************
REM
REM Get a named parameter list from this object
REM
REM ******************************************************

Function http_get_params(group="" As String)
    name = m.ParamGroup(group)
    if not m.DoesExist(name) then m[name] = NewUrlParams()
    return m[name]
End Function

REM ******************************************************
REM
REM Returns an associative array of cookie name/value pairs
REM which are extracted from the header response.
REM
REM Note: This only returns the cookie name/value pair and
REM       not the attributes following the name/value pair
REM       such as Domain or Expires fields. To retrieve those
REM       you will need to do additional parsing on the
REM       response headers array.
REM ******************************************************
Function http_get_cookies() As Object
    cookies = invalid
    if m.response <> invalid
        headers = m.response.getResponseHeadersArray()
        if headers <> invalid
            cookies = CreateObject("roAssociativeArray")
            for each header in headers
                cookieNameStart = 0
                cookieStr = header["Set-Cookie"]
                if cookieStr = invalid
                    cookieStr = header["Set-Cookie2"]
                end if
                if cookieStr <> invalid
                    cookieNameEnd = instr(1, cookieStr, "=")
                    if cookieNameEnd > 0
                        cookieName = mid(cookieStr, 1, cookieNameEnd - 1)
                        cookieValueStart = cookieNameEnd + 1 'one beyond the "="
                        cookieValueEnd = instr(cookieValueStart, cookieStr, ";")
                        cookieLength = cookieValueEnd - cookieValueStart
                        REM - cookieValue is a function; use value instead
                        value = Mid(cookieStr, cookieValueStart, cookieLength)
                        cookies[cookieName] = value
                    end if
                end if
            next
        end if
    end if
    return cookies
End Function

REM ******************************************************
REM
REM Return the full encoded URL.
REM
REM ******************************************************

Function http_get_url() As String
    url = m.base
    params = m.GetParams("urlParams")
    if not params.empty() then url = url + "?"
    url = url + params.encode()
    if m.anchor <> "" then url = url + "#" + m.anchor
    return url
End Function

REM ******************************************************
REM
REM Return the parameter group name,
REM correctly defaulted if necessary.
REM
REM ******************************************************

Function http_get_param_group(group="" as String)
    if group = ""
        if m.method="POST"
            name = "bodyParams"
        else
            name = "urlParams"
        end if
    else
        name = group
    end if
    return name
End Function

REM ******************************************************
REM
REM Performs Http.AsyncGetToString() in a retry loop
REM with exponential backoff. To the outside
REM world this appears as a synchronous API.
REM
REM Return empty string on timeout
REM
REM ******************************************************

Function http_get_to_string_with_retry() as String

    num_retries% = 5
    timeout%     = m.timeout / 3
    if timeout%<2 then timeout% = 2

    while num_retries% > 0
        ' print "Http: get tries left " + itostr(num_retries%)
        m.Prep("GET")
        if (m.Http.AsyncGetToString())
            if m.Wait(timeout%) then exit while
            timeout% = 2 * timeout%
            m.Http.EnableFreshConnection(true)
        endif
        num_retries% = num_retries% - 1
    end while

    if num_retries% < 1 then m.appLog(m.method + " timed out for " + m.sanitizeUrl(m.Http.GetURL()))

    return validstr(m.response) 'roUrlEvent supports ifString
End Function

REM ******************************************************
REM
REM Performs Http.AsyncGetToString() with a single timeout in seconds
REM To the outside world this appears as a synchronous API.
REM
REM Return empty string on timeout
REM
REM ******************************************************

Function http_get_to_string_with_timeout(seconds as Integer) as String
    m.Prep("GET")
    if (m.Http.AsyncGetToString()) then m.Wait(seconds)
    return validstr(m.response) 'roUrlEvent supports ifString
End Function

REM ******************************************************
REM
REM Performs Http.AsyncPostFromString() in a retry loop
REM with exponential backoff. To the outside
REM world this appears as a synchronous API.
REM
REM Return empty string on timeout
REM
REM ******************************************************

Function http_post_from_string_with_retry(val As String) as String

    num_retries% = 5
    timeout%     = m.timeout / 3
    if timeout%<2 then timeout% = 2


    while num_retries% > 0
        ' print "Http: get tries left " + itostr(num_retries%)
        m.Prep("POST")
        if (m.Http.AsyncPostFromString(val))
            if m.Wait(timeout%) then exit while
            timeout% = 2 * timeout%
            m.Http.EnableFreshConnection(true)
        endif
        num_retries% = num_retries% - 1
    end while

    if num_retries% < 1 then m.appLog(m.method + " timed out for " + m.sanitizeUrl(m.Http.GetURL()))

    return validstr(m.response) 'roUrlEvent supports ifString
End Function

REM ******************************************************
REM
REM Performs Http.AsyncPostFromString() with a single timeout in seconds
REM To the outside world this appears as a synchronous API.
REM
REM Return empty string on timeout
REM
REM ******************************************************

Function http_post_from_string_with_timeout(val As String, seconds as Integer) as String
    m.Prep("POST")
    if (m.Http.AsyncPostFromString(val)) then m.Wait(seconds)
    return validstr(m.response) 'roUrlEvent supports ifString
End Function

Function http_put_from_string_with_timeout(val As String, seconds as Integer) as String
    m.Prep("PUT")
    if (m.Http.AsyncPostFromString(val)) then m.Wait(seconds)
    return validstr(m.response) 'roUrlEvent supports ifString
End Function

Function http_delete_from_string_with_timeout(val As String, seconds as Integer) as String
    m.Prep("DELETE")
    if (m.Http.AsyncPostFromString(val)) then m.Wait(seconds)
    return validstr(m.response) 'roUrlEvent supports ifString
End Function

REM ******************************************************
REM
REM Performs Http.AsyncPostFromString() and returns immediately.
REM
REM ******************************************************

Sub http_async_post_from_string(val As String)
    m.Prep("POST")
    m.Http.AsyncPostFromString(val)
End Sub

REM ******************************************************
REM
REM Common wait() for all the synchronous http transfers
REM
REM @return True if the call completed successfully
REM         with HTTP 2XX or 3XX.
REM
REM ******************************************************

Function http_wait_with_timeout(seconds As Integer) As Boolean
    id = HttpActive().ID(m)
    while m.status=0
        nextTimeout = 1000 * seconds - m.timer.TotalMilliseconds()
        if seconds>0 and nextTimeout<=0 then exit while
        event = wait(nextTimeout, m.Http.GetPort())
        if type(event) = "roUrlEvent"
            HttpActive().receive(event)
            'sometimes roUrlEvent from other http objects come through
            'on the same port since some apps reuse a single port.
            'for example, the dispatcher port. should remove the id
            'corresponding to the received event but it's safer to just warn.
            eventID = event.GetSourceIdentity()
            if id <> eventID
                print "Http: WARNING -- waiting on ID #" id ", but received ID #" eventID "."
                print "Http:            this usually indicates that a sync call is using a shared port."
                print "Http:            dumping the intended HTTP target:"
                m.dump(true)
                eventHttp = HttpActive().getID(eventID)
                if eventHttp <> invalid
                    print "Http:            dumping the interloping HTTP response:"
                    eventHttp.dump(true)
                end if
                'we should be able to remove the eventID here but this can cause problems
                'for scripts which expect the request to remain alive. just warn the developer.
                'HttpActive().removeID(eventID)
            end if
        else if event = invalid
            m.appLog(m.method + " timed out for " + m.sanitizeUrl(m.Http.GetURL()))
            m.cancel()
        else
            print "Http: unhandled event "; type(event)
        endif
    end while
    HttpActive().removeID(id)
    'm.Dump()
    return m.Ok()
End Function

Function http_receive(msg As Object)
    m.status = msg.GetResponseCode()
    m.response = msg 'msg is a roUrlEvent
    m.label = "done"
    if isfunc(m.callbackReceive) then m.callbackReceive()
End Function

Function http_cancel()
    m.Http.AsyncCancel()
    m.status = -1
    m.label = "cancel"
    m.dump(true) 'true: truncate the response body
    HttpActive().remove(m)
End Function

Function http_go(method="" As String) As Boolean
    ok = false
    m.Prep(method)
    if m.method="POST" or m.method="PUT"
        ok = m.http.AsyncPostFromString(m.getParams().encode())
    else if m.method="GET" or m.method="DELETE" or m.method=""
        ok = m.http.AsyncGetToString()
    else
        print "Http: "; m.method; " is not supported"
    end if
    m.label = "sent"
    'm.Dump()
    return ok
End Function

Function http_ok() As Boolean
    ' depends on m.status which is updated by m.Wait()
    statusGroup = int(m.status/100)
    return statusGroup=2 or statusGroup=3
End Function

Function http_sync(seconds As Integer) As Boolean
    if m.Go() then m.Wait(seconds)
    return m.Ok()
End Function

REM ******************************************************
REM Dumps http debugging information.
REM ******************************************************
Function http_dump(truncateBodyResponse=false As Boolean) As Void
    time = "unknown"
    if m.DoesExist("timer") then time = itostr(m.timer.TotalMilliseconds())
    if m.Http <> invalid
        identity = m.Http.GetIdentity()
        url = m.Http.GetURL()
    else
        'm.Http can be invalid if the request hasn't been sent yet
        identity = "???"
        url = m.getUrl()
    end if
    print "Http: #"; identity; " "; m.label; " status:"; m.status; " time: "; time; "ms request: "; m.method; " "; url
    if not m.GetParams("bodyParams").empty() then print "  body: "; m.GetParams("bodyParams").encode()
    if truncateBodyResponse
        response = left(validstr(m.response), 1024)
    else
        response = m.response
    end if
    if isnonemptystr(m.response) then print "  response:"; chr(10); response
End Function

Function http_check_timeout(defaultTimeout=0 As Integer) As Integer
    timeLeft = m.timeout-m.timer.TotalMilliseconds()
    if timeLeft<=0
        ha = HttpActive()
        ha.addRetryToQueue()
        m.retry()
        timeLeft = defaultTimeout
    end if
    return timeLeft
End Function

Function http_retry(defaultTimeout=0 As Integer) As Integer
    m.cancel()
    if m.retries>0
        m.retries = m.retries - 1
        if isfunc(m.callbackRetry) then m.callbackRetry() else m.go()
    else if isfunc(m.callbackCancel)
        m.callbackCancel()
    end if
End Function

REM ******************************************************
REM
REM Two usage modes:
REM   'sets the key/value mapping & returns the value
REM   value = http.header(key, value) 
REM
REM   'just return the value mapped to the key
REM   value = http.header(key)
REM ******************************************************
Function http_header(key As String, value=invalid As Dynamic) As Dynamic
    ' used to control http headers
    h = m.headers
    if isstr(value) then h[key] = value
    return h[key]
End Function

REM ******************************************************
REM @return The request headers.
REM ******************************************************
Function http_get_request_headers() as String
    allHeaders = ""
    if type(m.headers) = "roAssociativeArray"
        for each key in m.headers
            allHeaders = allHeaders + key + ": " + validstr(m.headers[key]) + "; "
        end for
    end if
    return allHeaders
End Function

REM ******************************************************
REM @return The response headers.
REM ******************************************************
Function http_get_response_headers() as String
    allHeaders = ""
    if m.response <> invalid
        headers = m.response.getResponseHeaders()
        if headers <> invalid
            for each key in headers
                allHeaders = allHeaders + key + ": " + validstr(headers[key]) + "; "
            end for
        end if
    end if
    return allHeaders
End Function

REM ******************************************************
REM Logs the request & response to Roku's servers.
REM This should only be called once the response is complete.
REM ******************************************************
Function http_log_request_response() as Void
    id = itostr(m.http.GetIdentity())
    m.appLog("request " + id + " url: " + m.sanitizeUrl(m.http.getUrl()))
    m.appLog("request " + id + " headers: " + m.getRequestHeaders())
    m.appLog("response " + id + " headers: " + m.getResponseHeaders())
    if NOT m.ok()
        m.appLog("response " + id + " status: " + itostr(m.status))
    end if
    'truncate the response body down to 1024 characters to avoid overloading
    'the app logging system. that also matches the max server upload of the
    'native logging system.
    m.appLog("response " + id + " body: " + left(validstr(m.response), 1024))
End Function

REM ******************************************************
REM
REM Operations on a collection of URL parameters
REM
REM ******************************************************

Function NewUrlParams(encoded="" As String, separator="&" As String) As Object
    'stores the unencoded parameters in sorted order
    this                           = CreateObject("roAssociativeArray")
    this.names                     = CreateObject("roArray",0,true)
    this.params                    = CreateObject("roAssociativeArray")
    this.params.SetModeCaseSensitive()

    this.encode                    = params_encode
    this.parse                     = params_parse
    this.add                       = params_add
    this.addReplace                = params_add_replace
    this.addAll                    = params_add_all
    this.remove                    = params_remove
    this.empty                     = params_empty
    this.get                       = params_get
    this.separator                 = separator
    this.parse(encoded)
    return this
End Function

Function params_encode() As String
    encodedParams = ""
    m.names.reset()
    while m.names.isNext()
        name = m.names.Next()
        encodedParams = encodedParams + URLEncode(name) + "=" + URLEncode(m.params[name])
        if m.names.isNext() then encodedParams = encodedParams + m.separator
    end while
    return encodedParams
End Function

Function params_parse(encoded_params As String) as Object
    params = strTokenize(encoded_params,m.separator)
    for each paramExpr in params
        param = strTokenize(paramExpr,"=")
        if param.Count()=2 then m.addReplace(UrlDecode(param[0]),UrlDecode(param[1]))
    end for
    return m
End Function

Function params_add(name As String, val As String) as Object
    if not m.params.DoesExist(name)
        SortedInsert(m.names, name)
        m.params[name] = val
    end if
    return m
End Function

Function params_add_replace(name As String, val As String) as Object
    if m.params.DoesExist(name)
        m.params[name] = val
    else
        m.add(name,val)
    end if
    return m
End Function

Function params_add_all(keys as Object, vals as object) as Object
' keys is an array
' vals is an array
    i = 0
    for each name in keys
        if not m.params.DoesExist(name) then m.names.push(name)
        m.params[name] = vals[i]
        i = i + 1
    end for
    QuickSort(m.names)
    return m
End Function

sub params_remove(name As String)
    if m.params.delete(name)
    n = 0
    while n<m.names.count()
            if name=m.names[n]
                m.names.delete(n)
                return
            endif
            n = n + 1
    end while
    endif
End sub

Function params_empty() as Boolean
    return (m.params.IsEmpty())
End Function

Function params_get(name As String) as String
    return validstr(m.params[name])
End Function


REM ******************************************************
REM
REM URLEncode - strict URL encoding of a string
REM
REM ******************************************************

Function URLEncode(str As String) As String
    if not m.DoesExist("encodeProxyUrl") then m.encodeProxyUrl = CreateObject("roUrlTransfer")
    return m.encodeProxyUrl.urlEncode(str)
End Function

REM ******************************************************
REM
REM URLDecode - strict URL decoding of a string
REM
REM ******************************************************

Function URLDecode(str As String) As String
    strReplace(str,"+"," ") ' backward compatibility
    if not m.DoesExist("encodeProxyUrl") then m.encodeProxyUrl = CreateObject("roUrlTransfer")
    return m.encodeProxyUrl.Unescape(str)
End Function

REM ******************************************************
REM 
REM @param cookie A server cookie which may include additional
REM               attributes such as Path & Expiration. E.g.,
REM               "name=value; Expires=Wed, 09 Jun 2021 10:18:14"
REM @return The cookie value only. From above example it is just "value".
REM 
REM ******************************************************
Function cookieValue(cookie as String) As String
    equalPosition = instr(1, cookie, "=")
    if equalPosition > 0
        semicolonPosition = instr(1, cookie, ";")
        if semicolonPosition > 0
            'cookie attributes found.
            return mid(cookie, equalPosition + 1, semicolonPosition - equalPosition - 1)
        else
            'no cookie attributes found. just return the value
            return right(cookie, len(cookie) - equalPosition)
        end if
    else
        'no cookie value found
        return ""
    end if
End Function

'
' map of identity to active http objects
'
Function HttpActive() As Object
    ' singleton factory
    ha = m.HttpActive
    if ha=invalid
        ha = CreateObject("roAssociativeArray")
        ha.actives  = CreateObject("roAssociativeArray")
        ha.icount   = 0
        ha.count    = http_active_count
        ha.receive  = http_active_receive
        ha.defaultTimeout = 30000 ' 30 secs
        ha.checkTimeouts  = http_active_checkTimeouts

        ' by http obj
        ha.id       = http_active_id
        ha.add      = http_active_add
        ha.remove   = http_active_remove
        ha.replace  = http_active_replace

        ' by ID
        ha.getID    = http_active_getID
        ha.removeID = http_active_removeID
        ha.total    = strtoi(validstr(RegRead("Http.total","Debug")))

        ' by member existence
        ha.clearIf = http_active_clearIf

        ' timeout
        ha.retries          = CreateObject("roArray",10,true)
        ha.timer            = CreateObject("roTimespan")
        ha.addRetryToQueue  = http_retry_add_queue
        ha.getRetryCount    = http_get_retry_count
        ha.checkRetryQueue  = http_check_retry_queue
        ha.clearRetryQueue  = http_clear_retry_queue
        ha.retryQueueTime   = 60*1000  '60 secs
        ha.retryQueueCount  = 0

        m.HttpActive = ha
    end if
    return ha    
End Function

Function http_active_count() As Dynamic
    return m.icount
End Function

Function retryQueueCount() As Dynamic
    return m.retryQueueCount
End Function

Function http_retry_add_queue() 
    m.retries.Push(nowSecs())
    m.retryQueueCount = m.checkRetryQueue()
End Function

Function http_clear_retry_queue()
    if m.retries.Count() > 0 then
        m.retries.Clear()
        m.retryQueueCount = 0
    end if
End Function

Function http_check_retry_queue()
    if m.retries = invalid then return 0
    curTime = nowSecs()
    count = m.retries.Count()
    if count>0 then
        index = count - 1
        for i=0 to (count-1) 
            timeout = m.retries.GetEntry(index)
            if (curTime-timeout) > m.retryQueueTime then 
                m.retries.Delete(index)
            end if
            index = index - 1
            if index < 0 then exit for
        end for
        count = m.retries.Count()
    end if
    return count
End Function

Function http_get_retry_count()
    return m.retryQueueCount
End Function

Function http_default_app_log(msg As String) As Void
    print "Http: non-uploaded log: " msg
End Function

Function http_default_sanitize_url(url As String) As String
    return url
End Function

Function http_active_receive(msg As Object) As Dynamic
    id = msg.GetSourceIdentity()
    http = m.getID(id)

    if http<>invalid
        m.clearRetryQueue()
        http.receive(msg)
    else
        print "Http: #"; id; " discarding unidentifiable http response"
        print "Http: #"; id; " status"; msg.GetResponseCode()
        print "Http: #"; id; " response"; chr(10); msg.GetString()
    end if
    return http
end Function

REM ******************************************************
REM @return The ID extracted from the http object.
REM ******************************************************
Function http_active_id(http As Object) As Dynamic
    id = invalid
    if http.DoesExist("http")
        id = http.http.GetIdentity()
    end if
    'print "Http: got identity #"; id
    return id
End Function

Function http_active_add(http As Object)
    id = m.ID(http)
    if id<>invalid
        'print "Http: #"; id; " adding to active"
        m.actives[itostr(id)] = http
        m.icount = m.icount + 1
        m.total = m.total + 1
        if wrap(m.total,50)=0
            RegWrite("Http.total",itostr(m.total),"Debug")
            print "Http: total requests"; m.total
        end if
    end if
End Function

Function http_active_remove(http As Object)
    id = m.ID(http)
    if id<>invalid then m.removeID(id)
End Function

Function http_active_replace(http As Object, urlXfer As Object)
    m.remove(http)
    http.http = urlXfer
    m.add(http)
End Function

REM ******************************************************
REM @return The http object looked up by its ID as an integer.
REM ******************************************************
Function http_active_getID(id As Integer) As Dynamic
    return m.actives[itostr(id)]
End Function

REM ******************************************************
REM Removes the http object looked up by its ID as an integer.
REM ******************************************************
Function http_active_removeID(id As Integer)
    strID = itostr(id)
    if m.actives.DoesExist(strID)
        'print "Http: #"; id; " removing from active"
        m.actives.delete(strID)
        m.icount = m.icount -1
    end if
End Function

Function http_active_checkTimeouts() As Integer
    defaultTimeout = m.defaultTimeout
    timeLeft = defaultTimeout
    for each id in m.actives
        active = m.actives[id]
        activeTL = active.checkTimeout(defaultTimeout)
        if activeTL<timeLeft then timeLeft = activeTL
    end for
    return timeLeft
End Function

Function http_active_clearIf(obj As Dynamic, memberName="" As String)
    toDelete = CreateObject("roList")
    if isstr(obj)
        callback = false
    else ' assume obj is AA
        callback = true
        member = obj[memberName]
        tempFunc = "tempFunc"
        obj[tempFunc] = member ' so func can be called with correct m
    end if
    for each id in m.actives
        active = m.actives[id]
        if callback
            delThis = obj.tempFunc(active)
        else
            delThis = active.DoesExist(obj)
        end if
        if delThis then toDelete.push(active)
    end for
    if callback then obj.delete(tempFunc)
    for each active in toDelete
        id = m.ID(active)
        print "Http: removing active #"; id
        m.removeID(id)
    end for
End Function

' Other static methods

Function HttpXMLGet(url As String, requestXML As Object) As Object
    ' common sequence of ops to communicate via XML query-response over http
    gethttp = NewHttpXML(url)
    source = gethttp.PostFromStringWithRetry(requestXML.GenXML(false))
    resultXML = CreateObject("roXMLElement")
    resultXML.Parse(source)
    return resultXML
End Function


REM
REM Other utility functions required by Http
REM

' insert value into array
sub SortedInsert(A as object, value as string)
    count = a.count()
    a.push(value)       ' use push to make sure array size is correct now
    if count = 0
        return
    endif
    ' should do a binary search, but at least this is better than push and sort
    for i = count-1 to 0 step -1
        if value >= a[i]
            a[i+1] = value
            return
        endif
        a[i+1] = a[i]
    end for
    a[0] = value
end sub

REM  Can probably just replace this with mod operator that's available
REM  in BrightScript 3.0
Function wrap(num As Integer, size As Dynamic) As Integer
    ' wraps via mod if size works
    ' else just clips negatives to zero
    ' (sort of an indefinite size wrap where we assume
    '  size is at least num and punt with negatives)
    remainder = num
    if isint(size) and size<>0
        base = int(num/size)*size
        remainder = num - base
    else if num<0
        remainder = 0
    end if
    return remainder
End Function

