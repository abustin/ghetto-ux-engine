#ifndef _text_loader_h_
#define _text_loader_h_
#include <stdio.h>

#include <stdint.h>
#include <math.h>
#include <limits.h> /* PATH_MAX */
#include <stdlib.h>
#include "image_decoder.h"
void initText();
struct Bitmap *createText(char* text, int size, int lines, int width);

#endif