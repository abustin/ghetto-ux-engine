#include <png.h>
#include <jpeglib.h>
#include "image_decoder.h"
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdint.h>
#include <string.h>



static void png_read_data(png_structp ctx, png_bytep area, png_size_t size) {
    struct Mem *src;
    src = (struct Mem *)png_get_io_ptr(ctx);
    memcpy(area, src->mem + src->pos, size);
    src->pos += size;
}

struct my_error_mgr {
  struct jpeg_error_mgr pub;    /* "public" fields */

  jmp_buf setjmp_buffer;    /* for return to caller */
};

typedef struct my_error_mgr * my_error_ptr;

static void my_error_exit (j_common_ptr cinfo) {
  /* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
  my_error_ptr myerr = (my_error_ptr) cinfo->err;

  /* Always display the message. */
  /* We could postpone this until after returning, if we chose. */
  (*cinfo->err->output_message) (cinfo);

  /* Return control to the setjmp point */
  longjmp(myerr->setjmp_buffer, 1);
}


static struct Bitmap *decodeJPG(void *mem, int size) {
    unsigned long x, y;
    struct my_error_mgr jerr;
    unsigned long data_size;     // length of the file
    unsigned char * rowptr[1];  // pointer to an array
    unsigned char * jdata = NULL;      // data for the image
    struct Bitmap *bitmap = NULL;
    struct jpeg_decompress_struct info; //for our jpeg info
    struct jpeg_error_mgr err;          //the error handler

    
    /* We set up the normal JPEG error routines, then override error_exit. */
    info.err = jpeg_std_error(&jerr.pub);
    jerr.pub.error_exit = my_error_exit;
    /* Establish the setjmp return context for my_error_exit to use. */
    if (setjmp(jerr.setjmp_buffer)) {
        /* If we get here, the JPEG code has signaled an error.
         * We need to clean up the JPEG object, close the input file, and return.
         */
        jpeg_destroy_decompress(&info);
        if (jdata) {
            free(jdata);
        }
        if (bitmap) {
            free(bitmap);
        }
        return NULL;
    }

    jpeg_create_decompress(& info);   //fills info structure


    jpeg_mem_src(&info, mem, size);    
    int rc = jpeg_read_header(&info, TRUE);   // read jpeg file header


    if (rc != 1) {
        printf("File does not seem to be a normal JPEG");
        return NULL;
    }

    jpeg_start_decompress(&info);    // decompress the file

    //set width and height
    x = info.output_width;
    x = x - (x%4);
    //info.output_width = x;
    y = info.output_height;
    int pixel_size = info.output_components;
    int row_stride = x * pixel_size;

    data_size = x * y * pixel_size;

    //--------------------------------------------
    // read scanlines one at a time & put bytes 
    //    in jdata[] array. Assumes an RGB image
    //--------------------------------------------
    jdata = (unsigned char *)malloc(data_size+100);
    if (jdata == NULL) {
        puts("Wasn't able to alloc bitmap for jpeg decode");
    }

    int sl = 0;
    while (info.output_scanline < info.output_height) {

        rowptr[0] = jdata + info.output_scanline * row_stride;
        sl++;
        jpeg_read_scanlines(&info, rowptr, 1);
        
    }
    //---------------------------------------------------

    jpeg_finish_decompress(&info);   //finish decompressing
    jpeg_destroy_decompress(&info);
    bitmap = malloc(sizeof(struct Bitmap));
    bitmap->pitch = row_stride;
    bitmap->width = x;
    bitmap->height = y;
    bitmap->channels = pixel_size;
    bitmap->pixels = jdata;
    
    return bitmap;


}

static struct Bitmap *decodePNG(void *mem, int size) {


	struct Mem src = {mem, 0, size};

    
    const char *error;
    struct Bitmap *bitmap = NULL;
    
    png_structp png_ptr;
    png_infop info_ptr;
    png_uint_32 width, height;
    int bit_depth, color_type, interlace_type, num_channels;
    uint32_t Rmask;
    uint32_t Gmask;
    uint32_t Bmask;
    uint32_t Amask;
    //SDL_Palette *palette;
    png_bytep *volatile row_pointers;
    int row, i;
    int ckey = -1;
    png_color_16 *transv;

    if ( !mem ) {
        /* The error message has been set in SDL_RWFromFile */
        return NULL;
    }
    

    //if ( !IMG_Init(IMG_INIT_PNG) ) {
    //    return NULL;
    //}

    /* Initialize the data we will clean up when we're done */
    error = NULL;
    png_ptr = NULL; info_ptr = NULL; row_pointers = NULL; 

    /* Create the PNG loading context structure */
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,NULL,NULL,NULL);
    if (png_ptr == NULL){
        error = "Couldn't allocate memory for PNG file or incompatible PNG dll";
        goto done;
    }

     /* Allocate/initialize the memory for image information.  REQUIRED. */
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        error = "Couldn't create image information for PNG file";
        goto done;
    }

    /* Set error handling if you are using setjmp/longjmp method (this is
     * the normal method of doing things with libpng).  REQUIRED unless you
     * set up your own error handlers in png_create_read_struct() earlier.
     */

#ifdef __PI__
     #define LIBPNG_VERSION_12
#endif


#ifndef LIBPNG_VERSION_12
    if ( setjmp(*(jmp_buf *)(png_set_longjmp_fn(png_ptr, longjmp, sizeof (jmp_buf)))) )
#else
    if ( setjmp(png_ptr->jmpbuf) )
#endif
    {
        error = "Error reading the PNG file.";
        goto done;
    }

    /* Set up the input control */
    png_set_read_fn(png_ptr, &src, png_read_data);

    /* Read PNG header info */
    png_read_info(png_ptr, info_ptr);


    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, &interlace_type, NULL, NULL);

    /* tell libpng to strip 16 bit/color files down to 8 bits/color */
    png_set_strip_16(png_ptr) ;

    /* Extract multiple pixels with bit depths of 1, 2, and 4 from a single
     * byte into separate bytes (useful for paletted and grayscale images).
     */
    png_set_packing(png_ptr);

    /* scale greyscale values to the range 0..255 */
    if (color_type == PNG_COLOR_TYPE_GRAY)
        png_set_expand(png_ptr);

    /* For images with a single "transparent colour", set colour key;
       if more than one index has transparency, or if partially transparent
       entries exist, use full alpha channel */
    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) {
        int num_trans;
        uint8_t *trans;
        png_get_tRNS(png_ptr, info_ptr, &trans, &num_trans, &transv);
        if (color_type == PNG_COLOR_TYPE_PALETTE) {
            /* Check if all tRNS entries are opaque except one */
            int j, t = -1;
            for (j = 0; j < num_trans; j++) {
                if (trans[j] == 0) {
                    if (t >= 0) {
                        break;
                    }
                    t = j;
                } else if (trans[j] != 255) {
                    break;
                }
            }
            if (j == num_trans) {
                /* exactly one transparent index */
                ckey = t;
            } else {
                /* more than one transparent index, or translucency */
                png_set_expand(png_ptr);
            }
        } else {
            ckey = 0; /* actual value will be set later */
        }
    }

    if ( color_type == PNG_COLOR_TYPE_GRAY_ALPHA )
        png_set_gray_to_rgb(png_ptr);

    png_read_update_info(png_ptr, info_ptr);

    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, &interlace_type, NULL, NULL);

    /* Allocate the SDL surface to hold the image */
    Rmask = Gmask = Bmask = Amask = 0 ;
    num_channels = png_get_channels(png_ptr, info_ptr);

    if (num_channels < 3) {
        printf("Less than 3 channels %i\n", num_channels);
    }


    bitmap = (struct Bitmap*) malloc(sizeof(struct Bitmap));
    if (bitmap == NULL) {
        error = "Out of memory to bitmap";
        goto done;
    }
    bitmap->pitch = width*num_channels;
    bitmap->width = width;
    bitmap->height = height;
    bitmap->channels = num_channels;


    int mem_size = width*height*num_channels;

    bitmap->pixels = malloc(mem_size);
    if (bitmap->pixels == NULL) {
        error = "Out of memory to alloc pixels";
        goto done;
    }

    /* Create the array of pointers to image data */
    row_pointers = (png_bytep*) malloc(sizeof(png_bytep)*height);
    if (row_pointers == NULL) {
        error = "Out of memory";
        goto done;
    }
    for (row = 0; row < (int)height; row++) {
        row_pointers[row] = (png_bytep)
                (uint8_t *)bitmap->pixels + row*bitmap->pitch;
    }

    /* Read the entire image in one go */
    png_read_image(png_ptr, row_pointers);


done:   /* Clean up and return */
    

    if (png_ptr != NULL) {
        png_destroy_read_struct(&png_ptr,
                                info_ptr ? &info_ptr : (png_infopp)0,
                                (png_infopp)0);
    }
    if (row_pointers != NULL) {
        free(row_pointers);
    }
    if (error != NULL) {
        printf("%s\n", error);
        //SDL_RWseek(src, start, RW_SEEK_SET);
        if ( bitmap != NULL ) {
            if (bitmap->pixels) {
                free(bitmap->pixels);
                bitmap->pixels = NULL;
            }
            free(bitmap);
            bitmap = NULL;
        }
        //IMG_SetError(error);
    }
    return(bitmap);
}



struct Bitmap *decodeIMG(void *mem, int size) {
    unsigned char *data = (unsigned char*)mem;

    if (size > 4 && data[0] == 137 && data[1] == 'P' && data[2] == 'N' && data[3] == 'G') {
        return decodePNG(mem, size);
    } else if (size > 4 && data[0] == 0xFF && data[1] == 0xD8 && data[2] == 0xFF && (data[3] == 0xE0 || data[3] == 0xE1)) {
        return decodeJPG(mem, size);
    } else {
        printf("Unknown image format: %X %X %X %X\n", data[0],data[1],data[2],data[3]);
        return NULL;
    }

}

